currentPath=$PWD

mkdir -p /data/winkelmann
rsync -av /home/winkelmann/masterarbeit/ /data/winkelmann --exclude=".git" --exclude="Daten"

cd /data/winkelmann/MPCD
mkdir -p Daten
./main
python Skripte/Calculation/VelocityCorr.py 0.1&
