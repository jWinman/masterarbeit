make main

for i in 1 2 3 4;
do
    export OMP_NUM_THREADS=$i;
    echo "Cores: ${OMP_NUM_THREADS}";
    time ./main;
done;
