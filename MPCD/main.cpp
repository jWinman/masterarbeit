#include<algorithm>
#include <cmath>
#include <fstream>
#include <iostream>
#include <libconfig.h++>
#include <omp.h>
#include <random>
#include <string>
#include <vector>

#include "../libraries/mpc.h"
#include "../libraries/IOput.h"

int main(int argc, char* argv[])
{
    // Parameter einlesen
    libconfig::Config cfg;
    cfg.readFile("parameter.cfg");
    IOput IO = IOput(cfg, argc, argv[2], argv[argc - 1]);
    IO.writeConfig(cfg);

    // Simulationsgrößen
    int simTime = cfg.lookup("measurements.simTime");
    int equiTime = cfg.lookup("measurements.equiTime");
    int Nx = cfg.lookup("simulation.Nx");
    int Ny = cfg.lookup("simulation.Ny");
    int Nz = cfg.lookup("simulation.Nz");
    Eigen::Vector3d force;
    force[0] = cfg.lookup("measurements.forceX");
    force[1] = cfg.lookup("measurements.forceY");
    force[2] = cfg.lookup("measurements.forceZ");

    // Messgrößen
    std::vector<Eigen::Vector3d> momArray(simTime - equiTime);
    std::vector<double> energyArray(simTime - equiTime);
    std::vector<std::vector<Eigen::Vector3d>> VcmTopView(Nx, std::vector<Eigen::Vector3d>(Ny+1, Eigen::Vector3d::Zero()));
    std::vector<std::vector<Eigen::Vector3d>> VcmSideView(Ny+1, std::vector<Eigen::Vector3d>(Nz+1, Eigen::Vector3d::Zero()));

    // rd-Generator
    unsigned int seed = std::random_device()();
    printf("seed: %u\n", seed);
    std::vector<std::mt19937> generators(omp_get_max_threads());
    for (size_t i = 0; i < generators.size(); i++)
    {
    	generators[i] = std::mt19937(seed + i);
    }

    Mpc system = Mpc(generators, cfg);

    double time = 0;
    for (int t = 0; t < simTime; t++)
    {
    	system.reset_Parameter();
    	double tstart = omp_get_wtime();
    	system.v_center_of_mass();
    	system.MCthermostat(0.05);
    	system.collision();
    	system.streaming(force);
    	time += omp_get_wtime() - tstart;

	// Messungen
	system.VcmTopView(VcmTopView, simTime, Nz);
	system.VcmSideView(VcmSideView, simTime, Nx);
    	if (t >= equiTime)
    	{
	    momArray[t - equiTime] = system.momentum();
	    energyArray[t - equiTime] = system.kinE();
//	    IO.AppendToFile("velocities" + std::to_string(timeStep), system.getMPCparticles(), t - equiTime);
    	}
    }
    printf("%f \n", time);

    IO.print("energy", energyArray);
    IO.print("momentum", momArray);
    IO.printVelTopView(VcmTopView, Nx, Ny);
    IO.printVelSideView(VcmSideView, Ny, Nz);
    return 0;
}
