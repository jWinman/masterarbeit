import numpy as np
import sys
from scipy import signal
sys.path.append("../libraries")
import CfgReader

def autoCorr(vx, vy, vz):
    corr = np.correlate(vx, vx, "full") + np.correlate(vy, vy, "full") + np.correlate(vz, vz, "full")
    corr = corr[corr.size // 2:]
    return corr / corr[0]

cfg = CfgReader.CfgReader("parameter.cfg")
Nx, Ny, Nz = cfg.lookup("simulation.Nx"), cfg.lookup("simulation.Ny"), cfg.lookup("simulation.Nz")
M = cfg.lookup("mpc.M")
MPCparticles = int(Nx * Ny * Nz * M)
equiTime = cfg.lookup("measurements.equiTime")
simTime = cfg.lookup("measurements.simTime")

vx, vy, vz = np.loadtxt("Daten/velocities{0:.6f}.csv".format(float(sys.argv[1])), unpack=True)

vx = np.array(np.split(vx, simTime - equiTime)).T
vy = np.array(np.split(vy, simTime - equiTime)).T
vz = np.array(np.split(vz, simTime - equiTime)).T

autoCorr = np.array([np.arange(simTime - equiTime),
                     np.average(np.array([autoCorr(vx[i], vy[i], vz[i]) for i in range(MPCparticles)]),
                                axis=0)]).T

np.savetxt("Daten/corr{}.csv".format(sys.argv[1]), autoCorr, delimiter=' ', newline='\n')
