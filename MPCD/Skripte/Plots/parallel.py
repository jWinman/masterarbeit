import numpy as np
import matplotlib.pyplot as plt


fig = plt.figure()
ax = fig.add_subplot(1, 1, 1)

for i in range(1, 5):
    print(i)
    cores, time = np.loadtxt("Daten/parallel{}.txt".format(i), unpack=True)
    ax.plot(cores, time[0] / time, "o")
ax.set_xlabel(r"$N$ cores")
ax.set_ylabel(r"Speedup $S = \frac{T(1)}{T(N)}$")

fig.savefig("Plots/parallel.pdf")
