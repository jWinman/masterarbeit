import functools as fct
import numpy as np
import matplotlib.pyplot as plt
from matplotlib.backends.backend_pdf import PdfPages
from mpl_toolkits.mplot3d import Axes3D
import scipy.optimize as sc
from uncertainties import *
import sys
sys.path.append("../libraries")
import CfgReader

# Parameter einlesen
cfg = CfgReader.CfgReader("parameter.cfg")
Nx, Ny, Nz = cfg.lookup("simulation.Nx"), cfg.lookup("simulation.Ny"), cfg.lookup("simulation.Nz")
MPCparticles = Nx * Ny * Nz * cfg.lookup("mpc.M")
M = MPCparticles / (Nx * Ny * Nz)
Fx = cfg.lookup("measurements.forceX")
gradP = cfg.lookup("measurements.forceX") * MPCparticles / (Nx * Ny * Nz)
timeStep = cfg.lookup("mpc.timeStepMPC")

# Daten einlesen
x, y, ux, uy = np.loadtxt("Daten/VelTopView.csv", unpack=True)
norm = np.sqrt(ux**2 + uy**2)
x += 0.5
y += 0.5

pp = PdfPages("Plots/VelocityFields.pdf")

# Geschwindigkeitsfeld TOPVIEW
fig = plt.figure()
ax1 = fig.add_subplot(1, 1, 1)
ax1.set_title(r"velocity field $\bm{v}_{c, \mathrm{cm}}(x, y)$")
ax1.quiver(x, y, ux / norm, uy / norm)
ax1.set_xlabel(r"$x / a$")
ax1.set_ylabel(r"$y / a$")
norm = np.array(np.split(norm, Nx)).T
im = ax1.imshow(norm, extent=[0, Nx, 0, Ny], origin="lower", cmap="winter")
cb = fig.colorbar(im, ax = ax1, orientation="vertical")
cb.set_label(r"$v_{c,\mathrm{cm}} / \sqrt{\frac{k_{\mathrm{B}} T}{m}}$")
pp.savefig()

# Parabolisches Geschwindigkeitsprofil
# mean for ux
ux_mean = np.zeros(Ny)
for j in range(len(ux_mean)):
    mean = 0
    for i in range(Nx):
            mean += ux[i * len(ux_mean) + j] / Nx
    ux_mean[j] = mean

squareFunc = lambda y, eta, gradP, h: -1 / (2 * eta) * gradP * y * (y - h)
squareFunc = fct.partial(squareFunc, h=Ny, gradP=gradP)

ycorr = np.arange(0.5, len(ux_mean), 1.)
popt, pcov = sc.curve_fit(squareFunc, ycorr, ux_mean, [0.1])
etaSim = ufloat(popt[0], np.sqrt(pcov[0][0]))
y1 = np.linspace(0, Ny, 1000)

#Viskosität berechnen
print("Viskosität: ",  etaSim)
etaKin = MPCparticles * timeStep / (Nx * Ny * Nz) * (5 * M / ((M - 1) * (4 - 2 * np.cos(130 * 2 * np.pi / 360) - 2 * np.cos(2 * 130 * 2 * np.pi / 360))) - 0.5)
etaCol = MPCparticles / (18 * Nx * Ny * Nz * timeStep) * (1 - np.cos(130 * 2 * np.pi / 360)) * (1 - 1 / M)
eta = etaKin + etaCol
print("Berechnete Viskosität: ", etaKin + etaCol)

fig = plt.figure()
ax = fig.add_subplot(1, 1, 1)
ax.plot(ycorr, ux_mean, "+", label="SRD measurement")
ax.plot(y1, squareFunc(y1, etaSim.nominal_value), label=r"f\-it with $\eta = {:.1f} \pm {:.1f} \, \frac{{\sqrt{{k_{{\mathrm{{B}}}} T m}}}}{{a^2}}$".format(etaSim.nominal_value, etaSim.std_dev))
ax.set_title("velocity profile")
ax.legend(bbox_to_anchor=(0.9, -0.18))
ax.set_xlim(-1, Ny + 1)
#ax.set_ylim(-0.1, 2)
ax.set_xlabel(r"$y / a$")
ax.set_ylabel(r"$v_{c,\mathrm{cm}, x}(y) / \sqrt{\frac{k_{\mathrm{B}} T}{m}}$")
ax.grid()
pp.savefig()

# Geschwindigkeitsfeld SIDEVIEW
def fourierSeries(nmax, y, z, h, w, gradP, eta):
    series = [1 / n**3 * (1 - np.cosh(n * np.pi * (y - 0.5 * w) / h) / np.cosh(n * np.pi * w / (2 * h))) * np.sin(n * np.pi * z / h) for n in range(1, nmax, 2)]
    return 4 * h**2 * gradP / (np.pi**3 * eta) * sum(series)

# Daten einlesen
y, z, vx, _, _ = np.loadtxt("Daten/VelSideView.csv", unpack=True)
y += 0.5
z += 0.5
vxMatrix = np.array_split(vx, Ny)

# Scatter-Plot
plt.rcParams["savefig.bbox"] = "standard"
fig1 = plt.figure()
#ax1 = fig1.add_subplot(1, 1, 1, projection="3d")
ax1 = fig1.add_axes([0.01, 0.01, 0.88, 0.95], projection="3d")
ax1.scatter(y, z, vx, color="r", marker="+", label="SRD measurement")
ax1.scatter(y, z, fourierSeries(10, y, z, Ny, Nz, gradP, eta), color="b", marker="x", label="Fourier series")
ax1.set_xlabel(r"$y / a$")
ax1.set_ylabel(r"$z / a$")
ax1.legend(loc="upper center")
ax1.set_zlabel(r"$v_x(y, z) / \sqrt{\frac{k_{\mathrm{B}} T}{m}}$")
ax1.set_zlim(0, 0.4)
ax1.set_xlim(0, Nx)
ax1.set_ylim(0, Nz)
ax1.zaxis._axinfo['label']['space_factor'] = 3.2

for l in ax1.get_zticklabels():
    l.set_horizontalalignment("left")

for l in ax1.get_xticklabels():
    l.set_horizontalalignment("right")
    l.set_verticalalignment("center")

for l in ax1.get_yticklabels():
    l.set_verticalalignment("center")
pp.savefig()

# Imshow-Plot
plt.rcParams["savefig.bbox"] = "tight"
fig2 = plt.figure()
ax2 = fig2.add_subplot(1, 1, 1)
ax2.set_title(r"velocity field $\bm{v}_{c, \mathrm{cm}}(x, y)$")
ax2.set_xlabel(r"$y / a$")
ax2.set_ylabel(r"$z / a$")
ax2.imshow(vxMatrix, extent=[0, Ny, 0, Nz], origin="lower", cmap="winter")
cb = fig.colorbar(im, ax = ax2, orientation="vertical")
cb.set_label(r"$v_{c,\mathrm{cm}} / \sqrt{\frac{k_{\mathrm{B}} T}{m}}$")

pp.savefig()

pp.close()
