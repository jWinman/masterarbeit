import functools as ft
import numpy as np
import matplotlib.pyplot as plt
import scipy.optimize as sco
import sys
sys.path.append("../libraries")
import CfgReader

cfg = CfgReader.CfgReader("parameter.cfg")
M = cfg.lookup("mpc.M")
alpha = cfg.lookup("mpc.alpha")
gamma = 2. * (1 - np.cos(alpha * 2 * np.pi / 360)) / (3 * M) * (np.exp(-M) + M - 1)

Cfull = lambda gamma, n, a: (1 - gamma)**(a * n)
C = ft.partial(Cfull, gamma)

t1, corr1 = np.loadtxt("Daten/corr1.csv", unpack=True)
t01, corr01 = np.loadtxt("Daten/corr0.1.csv", unpack=True)
t01 *= 0.1

popt1, pcov1 = sco.curve_fit(C, t1[:3], corr1[:3])
a1 = popt1[0]
popt01, pcov01 = sco.curve_fit(C, t01[:3], corr01[:3])
a01 = popt01[0]
print(a1, a01)

fig = plt.figure(figsize=[4.15, 3.4])
ax = fig.add_subplot(1, 1, 1)

ax.plot(t1[:10], corr1[:10], "rx", label=r"$\langle x \rangle = 1\, \sqrt{\nicefrac{k_{\mathrm{B}} T}{m}}$")
ax.plot(t1[:10], C(t1[:10], a1), "r--")
ax.plot(t01[:100], corr01[:100], "bx", label=r"$\langle x \rangle = 0.1\, \sqrt{\nicefrac{k_{\mathrm{B}} T}{m}}$")
ax.plot(t01[:100], C(t01[:100], a01), "b--")
ax.set_xlabel(r"$t / \sqrt{\frac{ma^2}{k_{\mathrm{B}} T}}$")
ax.set_ylabel("$C_v(t)$")
ax.set_ylim(5e-5, 1)
ax.set_yscale("log")
ax.legend(loc="best")

fig.savefig("Plots/corr.pdf")
