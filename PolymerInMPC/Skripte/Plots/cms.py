import numpy as np
import matplotlib.pyplot as plt
from matplotlib.backends.backend_pdf import PdfPages

parameters = [("kappa", r"\kappa")]
nothing = [0]
widths = [4, 5, 6, 7, 8, 9]
kappas = [8, 10, 12, 14, 15, 16, 18, 20, 22, 24, 26, 28]
values = {".": nothing, "width": widths, "kappa": kappas}

for parameter, symbol in parameters:
    pp = PdfPages("Plots/cms-{}.pdf".format(parameter))
    array = values[parameter]
    for element in array:
        cmsX, cmsY, cmsZ = np.loadtxt("Daten/{}/cms{}.csv".format(parameter, element), unpack=True)
        t = np.arange(len(cmsX))

        print(np.mean(cmsX), np.mean(cmsY), np.mean(cmsZ))

        fig = plt.figure()
        ax1 = fig.add_subplot(3, 1, 1)
        ax1.set_title(r"${} = {}$".format(symbol, element))
        ax1.plot(t, cmsX)
        ax2 = fig.add_subplot(3, 1, 2)
        ax2.plot(t, cmsY)
        ax3 = fig.add_subplot(3, 1, 3)
        ax3.plot(t, cmsZ)
        pp.savefig()

pp.close()
