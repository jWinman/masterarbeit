import numpy as np
import matplotlib as mpl
import matplotlib.pyplot as plt
import linregress as lin
import sys
sys.path.append("../libraries")
import CfgReader

cfg = CfgReader.CfgReader("parameter.cfg")

t, MSD1NoHI = np.loadtxt("Daten/Correlation/MSD1NoHI.csv", unpack=True)
t, MSD1 = np.loadtxt("Daten/Correlation/MSD1.csv", unpack=True)
t, MSD = np.loadtxt("Daten/Correlation/MSD10.csv", unpack=True)
t, MSDNoHI = np.loadtxt("Daten/Correlation/MSD10NoHI.csv", unpack=True)
#t3, MSD3_1 = np.loadtxt("Daten/MSD/MSD3-1.csv", unpack=True)
#_, MSD3_2 = np.loadtxt("Daten/MSD/MSD3-2.csv", unpack=True)
#t5, MSD5_1 = np.loadtxt("Daten/MSD/MSD5-1.csv", unpack=True)
#_, MSD5_2 = np.loadtxt("Daten/MSD/MSD5-2.csv", unpack=True)

t *= cfg.lookup("mpc.timeStepMPC")
#t3 *= cfg.lookup("mpc.timeStepMPC")
#t5 *= cfg.lookup("mpc.timeStepMPC")

diff = lambda x, D: D * x
numPlot, numFit = 100, 50

a1, a_error1, b1, b_error1 = lin.linregress(t[:numFit], MSD1NoHI[:numFit])
print(a1, a_error1, b1, b_error1)
a1, a_error1, b1, b_error1 = lin.linregress(t[:numFit], MSD1[:numFit])
print(a1, a_error1, b1, b_error1)
a1, a_error1, b1, b_error1 = lin.linregress(t[:numFit], MSD[:numFit])
print(a1, a_error1, b1, b_error1)
a2, a_error2, b2, b_error2 = lin.linregress(t[:numFit], MSDNoHI[:numFit])
print(a2, a_error2, b2, b_error2)
#a2, a_error2, b2, b_error2 = lin.linregress(t[:numFit], MSD2[:numFit])
#print(a2, a_error2, b2, b_error2)

fig = plt.figure()
ax = fig.add_subplot(1, 1, 1)

ax.plot(t[:numPlot], MSD1NoHI[:numPlot], "+", label="one Polymer")
#ax.plot(t[:numPlot], MSD1[:numPlot], "+", label="one Polymer")
#ax.plot(t[:numPlot], MSD[:numPlot], "+", label="HI")
#ax.plot(t[:numPlot], diff(t[:numPlot], a1), "-", label="fit")
#ax.plot(t3[:numPlot], MSD3_2[:numPlot], "+", label="2 Polymers $d = 3\, a$")
ax.plot(t[:numPlot], MSDNoHI[:numPlot], "+", label="NoHI")
#ax.plot(t[:numPlot], diff(t[:numPlot], a2), "-", label="1 Polymer $d = 3\, a fit$")
#ax.plot(t5[:numPlot], MSD5_2[:numPlot], "+", label="2 Polymers $d = 5\, a$")
#ax.plot(t[:numPlot], linear(t[:numPlot], b2, a2), "b--")
#ax.set_yscale("log")
#ax.set_xscale("log")
ax.set_xlabel(r"$t / \sqrt{\frac{ma^2}{k_{\mathrm{B}} T}}$")
ax.set_ylabel(r"MSD")
ax.legend(loc="best")
ax.grid()

fig.savefig("Plots/MSDCompare.pdf")
