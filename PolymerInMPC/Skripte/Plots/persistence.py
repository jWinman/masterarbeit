import numpy as np
import matplotlib.pyplot as plt
from matplotlib.backends.backend_pdf import PdfPages
import scipy.optimize as sco
import uncertainties as un
import linregress as lr

# Parameters
parameters = [("kappa", r"\kappa", 9, 19), ("width", r"d", 40, 64)]
kappas = [8, 10, 12, 15, 16, 18, 20, 22, 24, 28]
widths = [5, 7, 9, 10, 12]
diction = {"kappa": kappas, "width": widths}

# Funktionen
kappa = lambda x, a, b: a * np.exp(b * x)
width = lambda x, defL: 1 - G(x, defL, 20) - G(x, defL, 20)
G = lambda x, defL, Lp: defL / (2 * np.sqrt(2) * Lp) * (np.cos(np.pi / 4) - np.cos(np.pi / 4 + x / defL) * np.exp(-x / defL))
functions = {"kappa": kappa, "width": width}
mastercurve = lambda x, a: a / (np.sqrt(2)) * (np.cos(np.pi/ 4) - np.cos(np.pi / 4 + x / a) * np.exp(-x / a))
linear = lambda x, a, b: x * a + b
pp = PdfPages("Plots/persistence.pdf")

for parameter, symbol, NumberOfBeadsFit, NumberOfBeadsPlot in parameters:

    # Variablen definieren
    variables = diction[parameter]
    Colors = plt.get_cmap("Dark2")(np.linspace(0, 1, len(variables)))
    korrelationFunc = functions[parameter]

    # TangentialKorrelationfkt
    fig = plt.figure(figsize=[8.3, 5])
    ax = fig.add_subplot(1 if parameter == "kappa" else 2, 1, 1)
    if (parameter == "width"):
        axMaster = fig.add_subplot(2, 1, 2)

    # Masterkurve
    Y, X, Y_error = np.zeros(len(variables)), np.zeros(len(variables)), np.zeros(len(variables))
    meanMasterX = np.zeros(NumberOfBeadsPlot)
    meanMasterY = np.zeros(NumberOfBeadsPlot)

    # Loop over all variables
    for i, (variable, Color) in enumerate(zip(variables, Colors)):
        s, korrelation = np.loadtxt("Daten/{}/tangentialKorr{}.csv".format(parameter, variable), unpack=True)
        x = np.linspace(0.001, NumberOfBeadsPlot, 1000)

        if (parameter == "width"):
            popt, pcov = sco.curve_fit(korrelationFunc, s[:NumberOfBeadsFit], korrelation[:NumberOfBeadsFit])
            defLb, defLb_error, Lp = popt[0], np.sqrt(pcov[0][0]), 20
            Y[i], Y_error[i], X[i] = defLb, defLb_error, Lp**(1 / 3) * variable**(2 / 3)
            print(variable, "Deflection length: ", round(defLb, 2), "Persistenzlänge: ", round(Lp, 2))
            ax.plot(x, korrelationFunc(x, defLb), color=Color, linestyle="-")
            ax.plot(s, korrelation, color=Color, marker=".", linestyle=" ", label=r"${} = {} \, a$".format(symbol, variable))
            meanMasterY += (1 - korrelation) * Lp**(2 / 3) * variable**(-2 / 3) / len(variables)
            meanMasterX += s / X[i] / len(variables)
            axMaster.plot(s / X[i], (1 - korrelation) * Lp**(2 / 3) * variable**(-2 / 3), color=Color, marker="o", linestyle="-", label=r"${} = {} \, a$".format(symbol, variable))

        elif (parameter == "kappa"):
            a, a_error, b, b_error = lr.linregress(s[:NumberOfBeadsFit], -np.log(korrelation[:NumberOfBeadsFit]))
            if (variable in [10, 12, 18, 22, 28]):
                ax.plot(x, korrelationFunc(x, 1, -a), color=Color, linestyle="-")
                ax.plot(s, korrelation, color=Color, marker="o", linestyle=" ", label=r"$L_p = {:.1f} \, a$".format(1 / a))

            print(variable, "Persistenzlänge: ", 1 / un.ufloat(a, a_error))
            Y[i], X[i] = 1 / a, variable

    ax.set_ylabel(r"$\langle\cos\theta(n)\rangle$")
    ax.set_xlabel(r"$n$")
    ax.set_ylim(0.5, 1)
    ax.set_xlim(0., max(x))
    ax.legend(loc="best", ncol=3)
    ax.set_title("TCF")

    # Masterkurve
    if (parameter == "width"):
        popt, pcov = sco.curve_fit(mastercurve, meanMasterX[:NumberOfBeadsFit], meanMasterY[:NumberOfBeadsFit])
        print("a: ", popt[0], pcov[0][0])

        axMaster.plot(meanMasterX, mastercurve(meanMasterX, popt[0]), "k", linewidth=3.5, label=r"$ c = {:.2f}$".format(popt[0]))
        axMaster.set_ylabel(r"$(1 - \langle \cos\theta(n) \rangle) {}_{{\phantom{{p}}}}^{{-\frac{{2}}{{3}}}} L_p^{{\frac{{2}}{{3}}}}$".format(symbol))
        axMaster.set_xlabel(r"$n {}_{{\phantom{{p}}}}^{{-\frac{{2}}{{3}}}} L_p^{{- \frac{{1}}{{3}}}} / a$".format(symbol))
        axMaster.legend(loc="best", ncol=2)
        axMaster.set_title("master curve")

    pp.savefig()

    a, a_error, b, b_error = lr.linregress(X, Y)
    print("Steigung: ", a, a_error)

    # persistence / deflection
    fig2 = plt.figure()
    ax2 = fig2.add_subplot(1, 1, 1)
    ax2.plot(X, Y, "ro")

    x = np.linspace(min(X) - 0.5, max(X) + 0.5, 100)

    if (parameter == "width"):
        ax2.plot(x, linear(x, a, b), label=r"$c = {:.2f} \pm {:.2f}$".format(a, a_error))
        ylabel = r"$\lambda / a$"
        xlabel = r"$L_p^{{\frac{{1}}{{3}}}} {}_{{\phantom{{p}}}}^{{\frac{{2}}{{3}}}} / a$".format(symbol)
        title = "Deflection length"
    elif (parameter == "kappa"):
        ax2.plot(x, linear(x, a, b), label=r"$L_p = ({:.2f} \pm {:.2f}) \, \frac{{\kappa}}{{k_{{\mathrm{{B}}}} T}}$".format(a, a_error))
        ylabel = r"$L_p / a$"
        xlabel = r"$\kappa / k_{\mathrm{B}} T a$"
        title = "persistence length"

    ax2.set_ylim(min(Y) - 0.1, max(Y) + 0.1)
    ax2.set_xlim(min(X) - 0.1, max(X) + 0.1)
    ax2.set_ylabel(ylabel)
    ax2.set_xlabel(xlabel)
    ax2.set_title(title)
    ax2.legend(loc="best")

    pp.savefig()

pp.close()
