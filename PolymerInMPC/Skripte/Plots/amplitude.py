import numpy as np
import matplotlib.pyplot as plt
from matplotlib.backends.backend_pdf import PdfPages
from scipy.optimize import curve_fit
import peakdetect as pd

L = np.array([1, 2, 3, 4, 5, 6, 7, 9, 10, 11, 12, 13, 14])
amplitude = np.zeros(len(L))
omegas = np.zeros(len(L))

for i, l in enumerate(L):
    t, aY1, aY2, cXY, cY = np.loadtxt("Daten/CorrelationFiniteSize/CrossCorrelation{}.csv".format(l), unpack=True)
    tmax, maxima = np.array(pd.peakdetect(aY1, t, 1000)[0]).T
    tmin, minima = np.array(pd.peakdetect(aY1, t, 1000)[1]).T
    amplitude[i] = max(aY1)
    omegas[i] = 2 * np.mean(np.diff(np.sort(np.concatenate((tmax, tmin)))))
    print("<Omega>: ", i, omegas[i])

powerLaw = lambda x, n, a: a * x**n
x = np.linspace(1., 10, 100)

popt, pcov = curve_fit(powerLaw, L, amplitude, [-1, 1])
n, a = popt[0], popt[1]
print("n:", n)
print("a:", a)

popt, pcov = curve_fit(powerLaw, L, omegas, [-1, 1])
nOmega, aOmega = popt[0], popt[1]
print("nOmega:", nOmega)
print("aOmega:", aOmega)

pp = PdfPages("Plots/Amplitude.pdf")
fig = plt.figure()
ax = fig.add_subplot(1, 1, 1)

ax.plot(L, omegas, "+", label="simulation results")
ax.plot(x, powerLaw(x, nOmega, aOmega), label=r"${:.1f}\left(\frac{{1}}{{N^m}}\right)^{{{:.2f}}}$".format(aOmega, -nOmega))
ax.set_ylabel(r"$\omega$")
ax.set_xlabel(r"$N^m$")
ax.legend(loc="best")

pp.savefig()

fig = plt.figure()
ax = fig.add_subplot(1, 1, 1)

ax.plot(L, amplitude, "+", label="simulation results")
ax.plot(x, powerLaw(x, n, a), label=r"${:.1f}\left(\frac{{1}}{{N^m}}\right)^{{{:.2f}}}$".format(a, -n))
ax.set_ylabel(r"$\hat{r}_{1y}$")
ax.set_xlabel(r"$N^m$")
ax.set_xlim(0.9, 10)
ax.legend(loc="best")
#ax.set_yscale("log")
#ax.set_xscale("log")

pp.savefig()
pp.close()
