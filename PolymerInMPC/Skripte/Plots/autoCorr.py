import numpy as np
import matplotlib.pyplot as plt
from matplotlib.backends.backend_pdf import PdfPages
import scipy.optimize as sc

pp = PdfPages("Plots/Correlation.pdf")

num1 = 3000
num2 = 3000
num2NoHI = 3000
steps1 = 100
steps2 = 100
steps2NoHI = 100

t1, distanceCorr1 = np.loadtxt("Daten/CorrelationL10/AutoCorrelationOnePolymer.csv", unpack=True)
t2, distanceCorr2 = np.loadtxt("Daten/CorrelationL10/AutoCorrelation10.csv", unpack=True)
t2NoHI, distanceCorrNoHI = np.loadtxt("Daten/CorrelationL10/AutoCorrelationNoHI.csv", unpack=True)
t1 *= 100 * 1e-3
t2 *= 100 * 1e-3
t2NoHI *= 100 * 1e-3

fig = plt.figure(figsize=[4.15, 3.5])
ax1 = fig.add_subplot(1, 1, 1)
ax1.plot(t1[0:num1:steps1], distanceCorr1[0:num1:steps1], "+")
ax1.set_ylabel(r"$\langle y(t') y(t' + t) \rangle$")
ax1.set_xlabel(r"$t / \, 10^3 \sqrt{\frac{ma^2}{k_{\mathrm{B}} T}}$")
ax1.grid()

pp.savefig()

fig = plt.figure(figsize=[4.15, 3.5])
ax1 = fig.add_subplot(1, 1, 1)

ax1.plot(t2NoHI[0:num2NoHI:steps2NoHI], distanceCorrNoHI[0:num2NoHI:steps2NoHI], "+", label="without HI")
ax1.plot(t2[0:num2:steps2], distanceCorr2[0:num2:steps2], "+", label="with HI")
ax1.set_ylabel(r"$\langle \Delta y(t') \Delta y(t' + t) \rangle$")
ax1.set_xlabel(r"$t / \, 10^3 \sqrt{\frac{ma^2}{k_{\mathrm{B}} T}}$")
ax1.grid()
ax1.legend(loc="best")

pp.savefig()
pp.close()
