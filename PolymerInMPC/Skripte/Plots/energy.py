import numpy as np
import matplotlib as mpl
import matplotlib.pyplot as plt
from matplotlib.backends.backend_pdf import PdfPages

parameters = [("width", "d")]
nothing = [0]
widths = [5, 6, 8]
kappas = [8, 10, 12, 14, 15, 16, 18, 20, 22, 24, 28]
values = {".": nothing, "kappa": kappas, "width": widths}

for parameter, symbol in parameters:
    pp = PdfPages("Plots/E-{}.pdf".format(parameter))
    array = values[parameter]
    for element in array:
        t, kinE, Epot, Eges = np.loadtxt("Daten/{}/energy{}.csv".format(parameter, element), unpack=True)
        print(np.mean(kinE))

        fig4 = plt.figure()
        ax = fig4.add_subplot(1, 1, 1)
        ax.set_title(r"${} = {}$".format(symbol, element))
        ax.plot(t, kinE, "r--", label="kinE")
        #ax.set_ylim(0.5, 3)
        #ax.plot(t, Epot, "b--", label="Epot")
        #ax.plot(t, Eges, "g--", label="Eges")
        ax.legend(loc="best")
        pp.savefig()

pp.close()
