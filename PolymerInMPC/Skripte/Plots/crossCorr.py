import numpy as np
import matplotlib.pyplot as plt
from functools import partial
from matplotlib.backends.backend_pdf import PdfPages

num1 = 1000
num2 = 5000
numberMono = [1, 3, 5, 9]

def plot(num, steps, Ls, Pfad, K):
    Colors = plt.get_cmap("spectral")(np.linspace(0, 0.8, len(Ls)))
    RH = 0.28
    d = 2.0
    eta = 82
    eps, xi = 3 / 4 * RH / d, 6 * np.pi * eta * RH
    scaling = 1e3**2 if (K == 1000) else 1

    theoretical11 = lambda t, eps, xi, k: 1 / (2 * k) * (np.exp(-t * (1 + eps) * k / xi) + np.exp(-t * (1 - eps) * k / xi)) * scaling
    theoretical12 = lambda t, eps, xi, k: 1 / (2 * k) * (np.exp(-t * (1 + eps) * k / xi) - np.exp(-t * (1 - eps) * k / xi)) * scaling
    theoretical11 = partial(theoretical11, eps=eps, xi=xi, k=K)
    theoretical12 = partial(theoretical12, eps=eps, xi=xi, k=K)

    pp = PdfPages("Plots/K={}.pdf".format(K))
    fig1 = plt.figure(figsize=[8.3, 10.5])
    ax11 = fig1.add_subplot(3, 1, 1)
    ax12 = fig1.add_subplot(3, 1, 2)
    ax21 = fig1.add_subplot(3, 1, 3)
    ax11.set_title(r"$K = {} \, \frac{{k_{{\mathrm{{B}}}}T}}{{a^2}}$".format(K))

    for l, Color in zip(Ls, Colors):
        t1, aY1, aY2, cXY, cY = np.loadtxt("Daten/{}/CrossCorrelation{}.csv".format(Pfad, l), unpack=True) * scaling
        t2, distanceCorr = np.loadtxt("Daten/{}/AutoCorrelation{}.csv".format(Pfad, l), unpack=True) * scaling
        t1 /= scaling
        t2 /= scaling

        ax11.plot(t1[0:num:steps], cY[0:num:steps], ".", color=Color, label=r"$\langle r_{{1, y}}(t') r_{{2, y}}(t' + t) \rangle$ for $N^m = {}$".format(l))

        ax12.plot(t1[0:num:steps], aY1[0:num:steps], ".", color=Color, label=r"$\langle r_{{1, y}}(t') r_{{1, y}}(t' + t) \rangle$ for $N^m = {}$".format(l))
        #ax12.plot(t1[0:num:steps], aY2[0:num:steps], ".", label=r"$\langle r_{{2, y}}(t') r_{{2, y}}(t' + t) \rangle$ for $N^m = {}$".format(l))


        ax21.plot(t2[0:num:steps], distanceCorr[0:num:steps], ".", color=Color, label=r"simulation for $N^m = {}$".format(l))
        ax21.plot(t1[0:num:steps], 2 * (aY1[0:num:steps] - cY[0:num:steps]), "--", color=Color, label=r"$2 \cdot ($ACF $-$ CCF) for $N^m = {}$".format(l))

    ax12.plot(t1[0:num:steps/2], theoretical11(t1[0:num:steps/2]), "-", linewidth=2.5, color="maroon", label=r"$K = {} \, \frac{{k_{{\mathrm{{B}}}} T}}{{a^2}}:$ theoretical prediction".format(K))
    lgnd = ax11.legend(loc="best", ncol=2)
    if (K == 50):
        ax11.set_ylim(-0.002, 0.001)
        ax11.plot(t1[0:num:steps/2], theoretical12(t1[0:num:steps/2]), "--", color=Colors[0], linewidth=2)
        ax11.set_ylabel(r"$\mathrm{{CCF}} / a^2$")
        ax12.set_ylabel(r"$\mathrm{{ACF}} / a^2$")
        ax21.set_ylabel(r"$\langle \Delta y(t') \Delta y(t' + t) \rangle / a^2$")
    else:
        ax11.set_ylabel(r"$\mathrm{{CCF}} / (10^{-3} \, a)^2$")
        ax12.set_ylabel(r"$\mathrm{{ACF}} / (10^{-3} \, a)^2$")
        ax21.set_ylabel(r"$\langle \Delta y(t') \Delta y(t' + t) \rangle / (10^{-3} \, a)^2$")

    ax11.grid()

    lgnd = ax12.legend(loc="best", ncol=2)
    ax12.grid()
    ax21.set_xlabel(r"$t / \sqrt{\frac{ma^2}{k_{\mathrm{B}} T}}$")
    ax21.grid()
    lgnd = ax21.legend(loc="best", ncol=2)

    pp.savefig()
    pp.close()

plot(num1, 10, numberMono, "Correlation", 1000)
plot(num2, 100, numberMono, "Correlationkext", 50)
