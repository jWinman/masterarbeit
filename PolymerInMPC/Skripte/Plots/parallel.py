import numpy as np
import matplotlib.pyplot as plt
from matplotlib.backends.backend_pdf import PdfPages

pp = PdfPages("Plots/parallel.pdf")

B = 0.85
amdahlsLaw = lambda B, N: 1 / ((1 - B) + 1 / N * B)
N = np.linspace(1, 30, 500)
N2 = np.linspace(1, 8, 100)

fig = plt.figure()
ax = fig.add_subplot(1, 1, 1)
ax.plot(N, amdahlsLaw(B, N), label="theoretical $S$")

ax2 = plt.axes([0.4, 0.2, 0.6, 0.4], axisbg="w")
ax2.plot(N2, amdahlsLaw(B, N2))

for i in range(1, 4):
    print(i)
    cores, time = np.loadtxt("Daten/Parallel/parallel{}.txt".format(i), unpack=True)
    if (i == 2):
        ax2.plot(cores, time[0] / time, "+", label="measured $S$")
        ax.plot(cores, time[0] / time, "+", label="measured $S$")
    else:
        ax2.plot(cores, time[0] / time, "+", label=" ")
        ax.plot(cores, time[0] / time, "+", label=" ")

ax.set_xlabel(r"$N_{\mathrm{cores}}$")
ax.set_ylabel(r"$S$")
ax.legend(loc="best")

pp.savefig()
pp.close()
