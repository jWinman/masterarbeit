import numpy as np
import sys
from scipy import signal

sys.path.append("../libraries")
import CfgReader

cfg = CfgReader.CfgReader("{0}/parameter{1}.cfg".format(sys.argv[1], sys.argv[2]))
timeStepMPC = cfg.lookup("mpc.timeStepMPC")
timeStepMD = cfg.lookup("polymer.timeStepMD")
writingNumber = cfg.lookup("measurements.writingNumber")

t, deltay = np.loadtxt("{0}/distance{1}.csv".format(sys.argv[1], sys.argv[2]), unpack=True)
t *= timeStepMPC * writingNumber

def AutoCorrelation(t, distance):
    distance -= np.mean(distance)
    corr = signal.fftconvolve(distance, distance[::-1])
    corr = np.array([i / (len(corr) - j) for j, i in enumerate(corr)])
    return np.array([t, corr[corr.size // 2:]]).T

numUpper = 30000000
num = 100
AutoCorr = AutoCorrelation(t, deltay)[0:numUpper:num]

np.savetxt("{0}/AutoCorrelation{1}.csv".format(sys.argv[1], sys.argv[2]), AutoCorr, delimiter=' ', newline='\n')
