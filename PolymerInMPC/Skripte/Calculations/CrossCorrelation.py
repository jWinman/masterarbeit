import numpy as np
import sys
from scipy import signal

sys.path.append("../libraries")
import CfgReader

cfg = CfgReader.CfgReader("{0}/parameter{1}.cfg".format(sys.argv[1], sys.argv[2]))
timeStepMPC = cfg.lookup("mpc.timeStepMPC")
timeStepMD = cfg.lookup("polymer.timeStepMD")
writingNumber = cfg.lookup("measurements.writingNumber")

def Corr(x1, x2):
    corr = signal.fftconvolve(x1, x2[::-1])
    corr = np.array([i / (len(corr) - j) for j, i in enumerate(corr)])
    return corr[corr.size // 2:]

def AllCorrelations(writingNumber, Upper, Steps):
    x1, y1, z1, x2, y2, z2 = np.loadtxt("{0}/CrossPosition{1}.csv".format(sys.argv[1], sys.argv[2]), unpack=True)
    t = np.arange(len(x1)) * timeStepMPC / writingNumber
    x1 -= np.mean(x1)
    y1 -= np.mean(y1)
    z1 -= np.mean(z1)
    x2 -= np.mean(x2)
    y2 -= np.mean(y2)
    z2 -= np.mean(z2)

    #xAutoCorr = Corr(x1, x1)
    y1AutoCorr = Corr(y1, y1)
    y2AutoCorr = Corr(y2, y2)
    #zAutoCorr = Corr(z1, z1)

    xyCorr = Corr(x1, y2)

    #xCrossCorr = Corr(x1, x2)
    yCrossCorr = Corr(y1, y2)
    #zCrossCorr = Corr(z1, z2)

    return np.array([t[0:Upper:Steps], y1AutoCorr[0:Upper:Steps], y2AutoCorr[0:Upper:Steps], xyCorr[0:Upper:Steps], yCrossCorr[0:Upper:Steps]]).T

Upper = 100000
Steps = 1
Corr = AllCorrelations(writingNumber, Upper, Steps)
np.savetxt("{0}/CrossCorrelation{1}.csv".format(sys.argv[1], sys.argv[2]), Corr, delimiter=' ', newline='\n')
