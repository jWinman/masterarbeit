#!/bin/bash

declare -a array
array=(4 5 6 7 8 9);

for i in ${array[@]};
do
	sed s/VARIABLE/$i/g < TangentialKorr_raw.sh > TangentialKorr.sh$i;
	qsub -vVAR="$1" TangentialKorr.sh$i;
done
