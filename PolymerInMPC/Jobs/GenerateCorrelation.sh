#!/bin/bash

declare -a array
array=(1 3 4 5 6 7 9);

for i in ${array[@]};
do
	sed s/VARIABLE/$i/g < Correlation_raw.sh > Correlation.sh$i;
	qsub -vVAR="$1" Correlation.sh$i;
done
