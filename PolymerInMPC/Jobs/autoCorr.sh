#!/bin/bash
# Carsten Raas <carsten.raas@tu-dortmund.de> / 2012-10-31

#------------------------------------------------------------------------------
# Note: "#PBS" is a queuing option, "##PBS" is a comment.
#------------------------------------------------------------------------------

#---------- Job name
#PBS -N PolyAutoCorr

#--------- Mail adress: Don't dare to use a wrong mail adress here. Two cakes!
#PBS -m n
#PBS -M jens.winkelmann@tu-dortmund.de
#PBS -m abe
#PBS -j oe

#--------- estimated (maximum) runtime (default: 1 hour)
#--------- [[hours:]minutes:]seconds[.milliseconds]
#PBS -l walltime=167:00:00

#---------- 1 core on one node (default: 1 core on 1 node)
#PBS -l nodes=1:ppn=8

#---------- Maximum amount of total virtual memory (default: 100 MB)
#PBS -l vmem=2gb

DIR="${PBS_O_WORKDIR}/../Daten/"

 cd ${PBS_O_WORKDIR}/../
 echo "${PBS_JOBNAME}"
 echo
 echo "      PBS_JOBID=${PBS_JOBID}"
 echo "    PBS_ARRAYID=${PBS_ARRAYID}"
 echo "PBS_ENVIRONMENT=${PBS_ENVIRONMENT}"
 echo "   PBS_NODEFILE=${PBS_NODEFILE}"
 echo "     PBS_SERVER=${PBS_SERVER}"

./main
python Skripte/Calculations/autoCorrelation2.py

 echo
 echo "$(whoami) is leaving from $(hostname) ..."
 echo
