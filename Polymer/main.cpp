#include <iostream>
#include <fstream>

#include "../libraries/polymer.h"
#include "../libraries/IOput.h"

//int main()
int main(int argc, char* argv[])
{
    libconfig::Config cfg;
    cfg.readFile("parameter.cfg");
    IOput IO = IOput(cfg, argc, argv[2], argv[argc - 1]);
    IO.setParams(cfg, argc, argv);
    IO.writeConfig(cfg);

    // Simulationsgrößen
    int equiTime = cfg.lookup("measurements.equiTime");
    int simTime = cfg.lookup("measurements.simTime");

    //unsigned int seed = std::random_device()();
    unsigned int seed = 205970369;
    printf("seed: %u\n", seed);
    std::vector<std::mt19937> generators(omp_get_max_threads());
    for (size_t i = 0; i < generators.size(); i++)
    {
    	generators[i] = std::mt19937(seed + i);
    }

    Polymere system(generators, cfg);

    std::vector<double> kinE(simTime - equiTime);
    std::vector<double> potE(simTime - equiTime);
    std::vector<Eigen::Vector3d> cms(simTime - equiTime);
    std::vector<double> distance(simTime - equiTime);

    system.calcForces();
    for (int t = 0; t < simTime; t++)
    {
	system.Verlet();
	if (t >= equiTime)
	{
	    int ttmp = t - equiTime;
	    system.OutputPos(ttmp);
	    kinE[ttmp] = system.Ekin();
	    potE[ttmp] = system.EPot();
    	    cms[ttmp] = system.CenterOfMass(0);
    	    //distance[ttmp] = system.distance(15, 15);
	}
    }

    IO.print("energy", kinE, potE);
    IO.print("cms", cms);
    //IO.print("distance", distance);

    return 0;
}
