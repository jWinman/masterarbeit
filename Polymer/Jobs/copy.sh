#! /bin/bash

echo "ssh cl1 'cd $1 && tar -cf Daten.tar Daten'"
ssh cl1 "cd $1 && tar -cf Daten.tar Daten"
echo "scp cl1:~/"$1"Daten.tar"
scp cl1:~/"$1"Daten.tar .
echo "ssh cl1 rm "$1"Daten.tar"
ssh cl1 rm "$1"Daten.tar
echo "tar -xzf Daten.tar -C $2"
tar -xzf Daten.tar -C $2
echo "rm Daten.tar"
rm Daten.tar
