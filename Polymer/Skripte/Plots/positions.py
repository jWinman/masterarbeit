import matplotlib as mpl
import matplotlib.pyplot as plt
import matplotlib.animation as animation
import numpy as np

boundaries = lambda x, l: x  - l * np.floor(x / l)

_, parameter = np.genfromtxt("parameter.ini", unpack=True)

Nx, Ny, Nz = parameter[4:7]
simTime = int(parameter[-1])

fig = plt.figure()
ax = fig.add_subplot(1, 1, 1)
ax.set_xlim(0, Nx)
ax.set_ylim(0, Ny)
line, = ax.plot([], [], "ro")

def init():
    line.set_data([], [])
    return line,

def animation(i):
    x, y, z = np.loadtxt("Daten/Distribution/positions-{}.csv".format(i), unpack=True)
    x, y, z = boundaries(x, Nx), boundaries(y, Ny), boundaries(z, Nz)
    line.set_data(x, y)
    return line,

anim = mpl.animation.FuncAnimation(fig, animation, init_func=init, frames=simTime)
anim.save("Plots/animation.mp4", fps=100)

