import numpy as np
import matplotlib.pyplot as plt
import scipy.optimize as sco
import uncertainties as un
import linregress as lr

fig = plt.figure()
ax = fig.add_subplot(1, 1, 1)

kappaArray = np.array([10])
Colors = ["r", "b", "k", "c", "m", "orange", "g", "maroon", "darkgreen", "grey", "purple", "yellowgreen", "peru"]

persistence = np.array([])

korrelationFunc = lambda x, a, b: a * np.exp(b * x)

for kappa, Color in zip(kappaArray, Colors):
    korrelation = np.loadtxt("Daten/tangentialKorr{}.csv".format(kappa), unpack=True)
    s = np.arange(len(korrelation))
    x = np.linspace(0, len(korrelation), 1000)

    params = [1, 1/ kappa]
    popt, pcov = sco.curve_fit(korrelationFunc, s, korrelation, params)
    a = un.ufloat(popt[0], pcov[0][0])
    b = un.ufloat(popt[1], pcov[1][1])
    persistence = np.append(persistence, - 1 / b.nominal_value)

    ax.plot(s, korrelation, color=Color, marker="o", linestyle=" ", label=r"$\kappa = {}$".format(kappa))
    ax.plot(x, korrelationFunc(x, a.nominal_value, b.nominal_value), color=Color, linestyle="-")
    ax.plot(x, korrelationFunc(x, 1, -1 / 10), color="b", linestyle="-")

ax.set_ylabel(r"$\langle \vec{b}_0 \cdot \vec{b}_n \rangle$")
ax.set_xlabel(r"$n$")
ax.set_ylim(-1, 2)
ax.legend(loc="best", ncol=3)
fig.savefig("Plots/TangentialKorr.pdf")

linear = lambda x, a, b: x * a + b
a, a_error, b, b_error = lr.linregress(kappaArray, persistence)
print(a)

fig2 = plt.figure()
ax2 = fig2.add_subplot(1, 1, 1)
#ax2.set_yscale("log")
#ax2.set_xscale("log")
ax2.set_ylabel(r"$L_p$")
ax2.set_xlabel(r"$\kappa$")

ax2.plot(kappaArray, persistence, "ro")
ax2.plot(kappaArray, linear(kappaArray, a, b))
fig2.savefig("Plots/persistence.pdf")
