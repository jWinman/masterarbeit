import numpy as np
import matplotlib.pyplot as plt


kappas = [0]

for kappa in kappas:
    cmsX, cmsY, cmsZ = np.loadtxt("Daten/cms{}.csv".format(kappa), unpack=True)
    t = np.arange(len(cmsX))

    fig = plt.figure()
    ax1 = fig.add_subplot(3, 1, 1)
    ax1.plot(t, cmsX)

    ax2 = fig.add_subplot(3, 1, 2)
    ax2.plot(t, cmsY)
    
    ax3 = fig.add_subplot(3, 1, 3)
    ax3.plot(t, cmsZ)
    fig.savefig("Plots/cms-{}.pdf".format(kappa))
