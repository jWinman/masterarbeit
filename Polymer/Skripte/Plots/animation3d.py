import numpy as np
from mayavi import mlab
import subprocess

import sys
sys.path.append("../libraries")
import CfgReader

# Parameter einlesen
cfg = CfgReader.CfgReader("parameter.cfg")
Nx = cfg.lookup("simulation.Nx")
Ny = cfg.lookup("simulation.Ny")
Nz = cfg.lookup("simulation.Nz")
polymers = cfg.lookup("polymer.polymers")
monomers = cfg.lookup("polymer.monomers")
simTime = cfg.lookup("measurements.simTime")
equiTime = cfg.lookup("measurements.equiTime")
abmessungen = [0, Nx, 0, Ny, 0, Nz]

# Daten einlesen
x, y, z = np.loadtxt("Daten/Animation/positions-0.csv", unpack=True)
boundaries = lambda x, l: x  - l * np.floor(x / l)

# Animation
figMayavi = mlab.figure(figure=1, bgcolor=(1, 1, 1), size=(200, 500))

# Box plotten
black = (0, 0, 0)
box1 = mlab.plot3d(np.arange(Nx), np.zeros(Nx), np.zeros(Nx), color=black)
box2 = mlab.plot3d(np.zeros(Ny), np.arange(Ny), np.zeros(Ny), color=black)
box3 = mlab.plot3d(np.zeros(Nz), np.zeros(Nz), np.arange(Nz), color=black)
box4 = mlab.plot3d(np.arange(Nx), np.zeros(Nx), np.zeros(Nx) + Nz, color=black)
box5 = mlab.plot3d(np.zeros(Nz) + Nx, np.zeros(Nz), np.arange(Nz), color=black)
box6 = mlab.plot3d(np.zeros(Nz), np.zeros(Nz) + Ny, np.arange(Nz), color=black)
box7 = mlab.plot3d(np.arange(Nx), np.zeros(Nx) + Ny, np.zeros(Nx), color=black)
box8 = mlab.plot3d(np.arange(Nx), np.zeros(Nx) + Ny, np.zeros(Nx) + Nz, color=black)
box9 = mlab.plot3d(np.zeros(Nz) + Nx, np.zeros(Nz) + Ny, np.arange(Nz), color=black)
box10 = mlab.plot3d(np.zeros(Ny) + Nx, np.arange(Ny), np.zeros(Ny) + Nz, color=black)
box11 = mlab.plot3d(np.zeros(Ny), np.arange(Ny), np.zeros(Ny) + Nz, color=black)
box12 = mlab.plot3d(np.zeros(Ny) + Nx, np.arange(Ny), np.zeros(Ny), color=black)
mlab.axes(extent=abmessungen)

#Monomere/Polymere plotten
filaments = []
ms = []
colors = [(1, 0, 0), (0, 0, 1)]
for p, Color in zip(range(polymers), colors):
    filaments.append(mlab.points3d(x[p * monomers:(p + 1) * monomers],
                                 y[p * monomers:(p + 1) * monomers],
                                 z[p * monomers:(p + 1) * monomers],
                                 resolution=40,
                                 scale_factor=0.25,
                                 color=Color))
    ms.append(filaments[-1].mlab_source)


subprocess.call("rm -rf Plots/Animation", shell=True)
subprocess.call("mkdir -p Plots/Animation", shell=True)
#Animationsschleife
for i in range(0, simTime // 3):
    mlab.view(azimuth=90, elevation=91, distance=7.5, roll=90, focalpoint=[5, 1.5, 4.5])
    x, y, z = np.loadtxt("Daten/Animation/positions-{}.csv".format(i), unpack=True)
    x, y, z = boundaries(x, Nx), boundaries(y, Ny), boundaries(z, Nz)
    for p in range(polymers):
        ms[p].set(x=x[p * monomers:(p + 1) * monomers],
                  y=y[p * monomers:(p + 1) * monomers],
                  z=z[p * monomers:(p + 1) * monomers])
    mlab.savefig("Plots/Distribution/position-{}.png".format(i))

subprocess.call("ffmpeg -r 30 -i Plots/Animation/position-%d.png -vf scale='trunc(iw/2)*2:trunc(ih/2)*2' -c:v libx264 -profile:v high -pix_fmt yuv420p -g 30 -r 60 Plots/polymer3d.mp4 -y", shell=True)

