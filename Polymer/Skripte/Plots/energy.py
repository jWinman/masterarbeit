import numpy as np
import matplotlib as mpl
import matplotlib.pyplot as plt

t, kinE, Epot, Eges = np.loadtxt("Daten/energy0.csv", unpack=True)

meanEges = np.mean(Eges)

fig4 = plt.figure()
ax = fig4.add_subplot(1, 1, 1)
ax.plot(t, kinE, "r--", label="kinE")
ax.plot(t, Epot, "b--", label="Epot")
ax.plot(t, Eges, "g--", label="Eges")
ax.plot(t, np.zeros(len(t)) + meanEges)
ax.legend(loc="best")
fig4.savefig("Plots/E.pdf")
