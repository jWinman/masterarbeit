import numpy as np
import sys

#jens: multi dimensional msd
def autoCorr(vecs, x_values):
   '''This function calculates autocorrelation function for a 1 d vec
   vecs: position values, needs to be type array(array) or list(array)
   x_values: array of dt to study
   '''
   m = np.array([])
   l = len(vecs)
   for t in x_values:
       tmp = 0
       n = l - t
       for i in xrange(0, n):
           tmp += vecs[t + i] * vecs[i]
       tmp /= n
       m = np.append(m,tmp)
   return x_values, m / m[0]

t, distance = np.loadtxt("Daten/distance0.csv", unpack=True)
autoCorr = np.array(autoCorr(distance, t.astype(int))).T

np.savetxt("Daten/autoCorrelation0.csv", autoCorr, delimiter=' ', newline='\n')
