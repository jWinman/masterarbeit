#include <iostream>
#include <fstream>
#include <libconfig.h++>
#include <omp.h>
#include <random>
#include <string>
#include <vector>

#include "../libraries/polymer.h"
#include "../libraries/mpc.h"
#include "../libraries/IOput.h"

#define CMS false
#define CROSS true
#define DISTANCE true
#define DESTROY_HI false

int main(int argc, char* argv[])
{
    // Parameter einlesen
    libconfig::Config cfg;
    cfg.readFile("parameter.cfg");
    IOput IO = IOput(cfg, argc, argv[2], argv[argc - 1]);
    IO.setParams(cfg, argc, argv);
    IO.writeConfig(cfg);

    // Simulationsgrößen
    double timeStepMPC = cfg.lookup("mpc.timeStepMPC");
    double timeStepMD = cfg.lookup("polymer.timeStepMD");
    int writingNumber = cfg.lookup("measurements.writingNumber");
    int equiTime = cfg.lookup("measurements.equiTime");
    int simTime = cfg.lookup("measurements.simTime");

    // Hilfsgrößen
    int MDtime = timeStepMPC / timeStepMD;
    Eigen::Vector3d force;
    force[0] = double(cfg.lookup("measurements.forceX"));
    force[1] = double(cfg.lookup("measurements.forceY"));
    force[2] = double(cfg.lookup("measurements.forceZ"));
    int polymers = cfg.lookup("polymer.polymers");
    int monomers = cfg.lookup("polymer.monomers");
    std::vector<Eigen::Vector3d> r(polymers * monomers);
    std::vector<Eigen::Vector3d> v(polymers * monomers);

    // Messgrößen
    std::vector<Eigen::VectorXd> CrossPos((simTime - equiTime) / writingNumber);
    std::vector<double> distance((simTime - equiTime) / writingNumber);
    std::vector<Eigen::Vector3d> cms((simTime - equiTime) / writingNumber);

    // Random Generator initialisieren
    //unsigned int seed = std::random_device()();
    unsigned int seed = 205970369;
    printf("seed: %u \n", seed);
    std::vector<std::mt19937> generators(omp_get_max_threads());
    for (size_t i = 0; i < generators.size(); i++)
    {
    	generators[i] = std::mt19937(seed + i);
    }

    // Erstellung der Objekte
    Polymere system(generators, cfg);
    Mpc mpc(generators, cfg);

    // Ausführung des MPC/MD-Algorithmus
    system.calcForces();
    for (int t = 0; t < simTime; t++)
    {
	std::tie(r, v) = system.GetPosVel();
	mpc.setMDparticles(r, v);

    	mpc.reset_Parameter();
    	mpc.v_center_of_mass();
    	mpc.MCthermostat(0.2);
    	mpc.collision();
    	if (DESTROY_HI) mpc.destroy_HI();
    	mpc.streaming(force);

    	system.SetVel(mpc.getMDparticles());

    	for (int tMD = 0; tMD < MDtime; tMD++)
    	{
	    system.Verlet();
	}

	// Messungen
	if (t >= equiTime)
	{
	    int ttmp = t - equiTime;
	    if (ttmp % writingNumber == 0)
	    {
	    	if (CROSS) CrossPos[ttmp / writingNumber] = system.CrossCorr(0, 0);
		if (DISTANCE) distance[ttmp / writingNumber] = system.distance(0, 0);
		if (CMS) cms[ttmp / writingNumber] = system.CenterOfMass(0);
	    }
	}
    }

    //Output
    if (CMS) IO.print("cms", cms);
    if (CROSS) IO.print("CrossPosition", CrossPos);
    if (DISTANCE) IO.print("distance", distance);

    return 0;
}
