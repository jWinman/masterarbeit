#!/bin/bash

declare -a array
array=(8 9);

for i in ${array[@]};
do
	sed s/VARIABLE/$i/g < FiniteSize_raw.sh > FiniteSize.sh$i;
	qsub -vVAR="$1" FiniteSize.sh$i;
done
