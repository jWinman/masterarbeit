currentPath=$PWD

mkdir -p /data/winkelmann
rsync -av /home/winkelmann/masterarbeit/ /data/winkelmann --exclude=".git" --exclude="Daten"

cd /data/winkelmann/MonomersInMPC
mkdir -p $3
./main $1 $2 $3
python Skripte/Calculations/CrossCorrelation.py $3 $2
python Skripte/Calculations/AutoCorrelation.py $3 $2
#python Skripte/Calculations/MSD.py $3 $2

#cp -r $3/* $currentPath/$3/
#cd /data
#rm -rf winkelmann
