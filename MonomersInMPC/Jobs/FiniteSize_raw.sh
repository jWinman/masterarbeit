#!/bin/bash
# Carsten Raas <carsten.raas@tu-dortmund.de> / 2012-10-31

#------------------------------------------------------------------------------
# Note: "#PBS" is a queuing option, "##PBS" is a comment.
#------------------------------------------------------------------------------

#---------- Job name
#PBS -N FINITESIZE-VARIABLE

#--------- Mail adress: Don't dare to use a wrong mail adress here. Two cakes!
#PBS -m n
#PBS -M jens.winkelmann@tu-dortmund.de
#PBS -m abe
#PBS -j oe

#--------- estimated (maximum) runtime (default: 1 hour)
#--------- [[hours:]minutes:]seconds[.milliseconds]
#PBS -l walltime=6:00:00

#---------- 1 core on one node (default: 1 core on 1 node)
#PBS -l nodes=1:ppn=8

#---------- Maximum amount of total virtual memory (default: 100 MB)
#PBS -l vmem=12gb
#PBS -q th123_node

export OMP_NUM_THREADS=${PBS_NUM_PPN}

DIR="${PBS_O_WORKDIR}/../${VAR}"
LOCAL="/SCRATCH/${USER}/${PBS_JOBID}"

mkdir -p ${DIR}
mkdir -p ${LOCAL}

 cd ${PBS_O_WORKDIR}/../
 echo "${PBS_JOBNAME}"
 echo
 echo "      PBS_JOBID=${PBS_JOBID}"
 echo "    PBS_ARRAYID=${PBS_ARRAYID}"
 echo "PBS_ENVIRONMENT=${PBS_ENVIRONMENT}"
 echo "   PBS_NODEFILE=${PBS_NODEFILE}"
 echo "     PBS_SERVER=${PBS_SERVER}"

 ./main simulation.Nx VARIABLE simulation.Ny VARIABLE simulation.Nz VARIABLE ${LOCAL}
# python2.7 Skripte/Calculations/CrossCorrelation.py ${LOCAL} VARIABLE
 python2.7 Skripte/Calculations/AutoCorrelation.py ${LOCAL} VARIABLE

# mv ${LOCAL}/CrossCorrelationVARIABLE.csv ${DIR}
 mv ${LOCAL}/AutoCorrelationVARIABLE.csv ${DIR}
 mv ${LOCAL}/parameterVARIABLE.cfg ${DIR}
 rm -r ${LOCAL}
 #rmdir --ignore-fail-on-non-empty /SCRATCH/${USER}

 echo
 echo "$(whoami) is leaving from $(hostname) ..."
 echo
