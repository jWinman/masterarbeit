#!/bin/bash

declare -a array
array=(10 15 20 25 30 40);

for i in ${array[@]};
do
	sed s/VARIABLE/$i/g < Correlation_raw.sh > Correlation.sh$i;
	qsub -vVAR="$1" Correlation.sh$i;
done
