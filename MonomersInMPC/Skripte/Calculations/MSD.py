import numpy as np
import sys

#jens: multi dimensional msd
def msdisplacemd(vecs, x_values):
   '''This function calculates mean squared displacement for a 1 d vec
   vecs: position values, needs to be type array(array) or list(array)
   x_values array of dt to study
   '''
   m = np.array([])
   l = len(vecs)
   for t in x_values:
       tmp = 0
       n = l - t
       for i in xrange(0,n):
           tmp += np.dot((vecs[t+i]-vecs[i]),(vecs[t+i]-vecs[i]))
       tmp /= n
       m = np.append(m,tmp)
   return x_values, m

centerOfMass = np.loadtxt("{0}/cms{1}.csv".format(sys.argv[1], sys.argv[2]))
t = np.concatenate([np.arange(0, 100, 2), np.arange(100, 10000, 100)])

MSD = np.array(msdisplacemd(centerOfMass, t))

np.savetxt("{0}/MSD{1}.csv".format(sys.argv[1], sys.argv[2]), MSD.T)
