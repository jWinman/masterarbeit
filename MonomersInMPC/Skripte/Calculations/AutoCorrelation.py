import numpy as np
import threading as thread
import sys
from scipy import signal

sys.path.append("../libraries")
import CfgReader

cfg = CfgReader.CfgReader("{0}/parameter{1}.cfg".format(sys.argv[1], sys.argv[2]))
timeStepMPC = cfg.lookup("mpc.timeStepMPC")
timeStepMD = cfg.lookup("polymer.timeStepMD")

_, deltay = np.loadtxt("{0}/distance{1}.csv".format(sys.argv[1], sys.argv[2]), unpack=True)

def AutoCorrelation(distance):
    distance -= np.mean(distance)
    corr = signal.fftconvolve(distance, distance[::-1])
    corr = np.array([i / (len(corr) - j) for j, i in enumerate(corr)])
    return corr[corr.size // 2:]

num = 100000
AutoCorr = AutoCorrelation(deltay)[:num].T

np.savetxt("{0}/AutoCorrelation{1}.csv".format(sys.argv[1], sys.argv[2]), AutoCorr, delimiter=' ', newline='\n')
