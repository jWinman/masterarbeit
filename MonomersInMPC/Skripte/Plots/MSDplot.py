import numpy as np
import matplotlib as mpl
import matplotlib.pyplot as plt
import linregress as lin

linear = lambda t, D, a: D * t + a

fig = plt.figure()
ax = fig.add_subplot(1, 1, 1)

num = 1000
t, MSD = np.loadtxt("Daten/MSD1.csv", unpack=True)
print(len(MSD))
t *= 0.01
a, a_error, b, b_error = lin.linregress(t[:num], MSD[:num])
D, D_error = (a / 6, a_error / 6)
print("Diffusionskonstante: ", D, D_error)

tNoHI, MSDNoHI = np.loadtxt("Daten/MSD1NoHI.csv", unpack=True)
print(len(MSDNoHI))
tNoHI *= 0.01
aNoHI, a_errorNoHI, bNoHI, b_errorNoHI = lin.linregress(t[:num], MSDNoHI[:num])
DNoHI, D_errorNoHI = (aNoHI / 6, a_errorNoHI / 6)
print("Diffusionskonstante NoHI: ", DNoHI, D_errorNoHI)

ax.plot(t[:num], MSD[:num], ".", label="HI")
ax.plot(tNoHI[:num], MSDNoHI[:num], ".", label="NoHI")
ax.plot(t[:num], linear(t[:num], a, b), "--")
ax.plot(t[:num], linear(t[:num], aNoHI, bNoHI), "--")
ax.set_xlabel(r"$t / \sqrt{\frac{ma^2}{k_{\mathrm{B}} T}}$")
ax.set_ylabel(r"$\langle (\vec{r}(t_0) - \vec{r}(t))^2 \rangle / a^2$")

ax.legend(loc="best")
fig.savefig("Plots/MSD.pdf")

