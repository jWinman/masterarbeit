import numpy as np
import matplotlib.pyplot as plt
from matplotlib.backends.backend_pdf import PdfPages
from mpl_toolkits.mplot3d import Axes3D
import scipy.optimize as sc
from uncertainties import *
import sys
sys.path.append("../libraries")
import CfgReader

# Parameter einlesen
cfg = CfgReader.CfgReader("parameter.cfg")
Nx, Ny, Nz = cfg.lookup("simulation.Nx"), cfg.lookup("simulation.Ny"), cfg.lookup("simulation.Nz")
MPCparticles = Nx * Ny * Nz * cfg.lookup("mpc.M")
Fx = cfg.lookup("measurements.forceX")
gradP = cfg.lookup("measurements.forceX") * MPCparticles / (Nx * Ny * Nz)

# Daten einlesen
x, y, ux, uy = np.loadtxt("Daten/VelTopView.csv", unpack=True)
norm = np.sqrt(ux**2 + uy**2)

pp = PdfPages("Plots/VelocityFields.pdf")

# Geschwindigkeitsfeld TOPVIEW
fig = plt.figure()
ax1 = fig.add_subplot(1, 1, 1)
ax1.set_title("Geschwindigkeitsfeld")
ax1.quiver(x, y, ux / norm, uy / norm, norm, cmap='spring', scale_units="xy", scale=1)
ax1.set_xlabel("$x / a$")
ax1.set_ylabel("$y / a$")
norm = np.array(np.split(norm, x[-1] + 1)).T
im = ax1.imshow(norm, origin="lower")
cb = fig.colorbar(im, ax = ax1, orientation="horizontal")
cb.set_label(r"$v_{c,\mathrm{cm}} / \sqrt{\frac{k_{\mathrm{B}} T}{m}}$")
pp.savefig()

# Geschwindigkeitsfeld SIDEVIEW
# Daten einlesen
y, z, vx, vy, vz = np.loadtxt("Daten/VelSideView.csv", unpack=True)
norm = np.sqrt(vy**2 + vz**2)

# Scatter-Plot
fig1 = plt.figure()
ax1 = fig1.add_subplot(1, 1, 1, projection="3d")
ax1.scatter(y, z, vx, color="r", marker="+")
ax1.set_xlabel("$y / a$")
ax1.set_ylabel("$z / a$")
ax1.set_zlabel(r"$v_x / \sqrt{\frac{k_{\mathrm{B}} T}{m}}$")
pp.savefig()

# Imshow-Plot
fig2 = plt.figure()
ax2 = fig2.add_subplot(1, 1, 1)
ax2.quiver(y, z, vy / norm, vz / norm, norm, cmap='spring', scale_units="xy", scale=1)
ax2.set_xlabel("$y / a$")
ax2.set_ylabel("$z / a$")
norm = np.array(np.split(norm, y[-1] + 1)).T
ax2.imshow(norm, origin="lower")
cb = fig.colorbar(im, ax = ax2, orientation="vertical")

pp.savefig()

pp.close()

_, distance = np.loadtxt("Daten/distance0.csv", unpack=True)

print(np.mean(distance))
