import numpy as np
import matplotlib.pyplot as plt
from scipy.optimize import curve_fit
from functools import partial
import peakdetect as pd

ks = np.array([5, 10, 15, 20, 25, 30, 40, 50, 60, 70, 80, 90, 100, 120, 140])
corr = np.zeros(len(ks))
tmins = np.zeros(len(ks))

for i, k in enumerate(ks):
    aY1, aY2, cXY, cY = np.loadtxt("Daten/kext/CrossCorrelation{}.csv".format(k), unpack=True)
    t = np.arange(len(cY)) * 0.01
    tmin, minima = np.array(pd.peakdetect(cY, t, 10)[1]).T
    tmins[i] = tmin[0]
    corr[i] = minima[0]

D = 0.0023
xi = 1 / D
eps = 3 / 4 * 0.28 / 2
tau_i = lambda k, xi: xi / k
tau_i = partial(tau_i, xi=xi)
corr_i = partial(tau_i, xi=-np.sinh(eps) / np.e)
x = np.linspace(min(ks), max(ks), 100)

fig = plt.figure()
ax = fig.add_subplot(2, 1, 1)
ax2 = fig.add_subplot(2, 1, 2)

ax.plot(ks, tmins, "o", label="simulation results")
ax.plot(x, tau_i(x), "-", label="theoretical approximation")
ax.set_ylabel(r"$t_{\mathrm{min}} / \frac{ma^2}{k_{\mathrm{B}} T}$")
ax.axvspan(0, 20, alpha=0.4, color="red")
ax.legend(loc="best")

ax2.plot(ks, corr, "o", label="simulation results")
ax2.plot(x, corr_i(x), "-", label="theoretical approximation")
ax2.set_ylabel(r"$\langle r_{1y}(t') r_{1y}(t' + t_{\mathrm{min}}) \rangle / a^2$")
ax2.set_xlabel(r"$K / \frac{k_{\mathrm{B}} T}{a^2}$")
ax2.legend(loc="best")
ax2.axvspan(0, 20, alpha=0.4, color="red")
ax2.set_ylim(-0.005, 0)

fig.savefig("Plots/CorrelationTime.pdf")
