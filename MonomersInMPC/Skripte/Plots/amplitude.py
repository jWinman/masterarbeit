import numpy as np
import matplotlib.pyplot as plt
from matplotlib.backends.backend_pdf import PdfPages
import linregress as lin
import peakdetect as pd
import uncertainties as un

L = np.array([5, 6, 7, 8, 9, 10, 11, 12, 13, 14, 15, 16, 17, 18])
omegas = np.zeros(len(L))
omegaErrors = np.zeros(len(L))

for i, l in enumerate(L):
    aY1 = np.loadtxt("Daten/LFiniteSize/AutoCorrelation{}.csv".format(l), unpack=True)
    t = np.arange(len(aY1)) * 1e-2
    tmax, maxima = np.array(pd.peakdetect(aY1[:350000], t[:350000], 10000, 0.2)[0]).T
    tmin, minima = np.array(pd.peakdetect(aY1[:350000], t[:350000], 10000, 0.2)[1]).T
    omegas[i] = 2 * np.mean(np.diff(np.sort(np.concatenate((tmax, tmin)))))
    omegaErrors[i] = 2 * np.std(np.diff(np.sort(np.concatenate((tmax, tmin)))))
    print("<Omega>: ", l, omegas[i], omegaErrors[i])

linear = lambda x, a, b: a * x + b
x = np.linspace(2., 20, 100)

a, a_error, b, b_error = lin.linregress(L, omegas)
aOmega = un.ufloat(a, a_error)
bOmega = un.ufloat(b, b_error)

print("aOmega:", aOmega)
print("bOmega:", bOmega)

pp = PdfPages("Plots/Frequency.pdf")
fig = plt.figure(figsize=[8.3, 3.5])
ax = fig.add_subplot(1, 1, 1)

ax.errorbar(L, omegas, yerr=omegaErrors, fmt=".", label="simulation results")
ax.plot(x, linear(x, aOmega.nominal_value, bOmega.nominal_value), label=r"$T(L) = ({:.0f} \pm {:.0f}) \, \sqrt{{\frac{{m}}{{k_{{\mathrm{{B}}}} T}}}} \, L + ({:.0f} \pm {:.0f}) \, \sqrt{{\frac{{ma^2}}{{k_{{\mathrm{{B}}}} T}}}}$".format(aOmega.nominal_value, aOmega.std_dev, bOmega.nominal_value, bOmega.std_dev))
ax.set_ylabel(r"$T / \sqrt{\frac{ma^2}{k_{\mathrm{B}} T}}$")
ax.set_xlabel(r"$L / a$")
ax.set_ylim(0, linear(20, aOmega.nominal_value, bOmega.nominal_value))
ax.legend(loc="best")

pp.savefig()
pp.close()
