import numpy as np
import matplotlib.pyplot as plt
from matplotlib.backends.backend_pdf import PdfPages
import peakdetect as pd
from functools import partial

num = 5000
steps = 100
ks = [50, 80, 100]
ksNoHI = [50]
ksFiniteSize = [1]

def plot(num, steps, ks, Pfad, Name):
    theoretical11 = lambda t, eps, xi, k: 1 / (2 * k) * (np.exp(-t * (1 + eps) * k / xi) + np.exp(-t * (1 - eps) * k / xi))
    theoretical12 = lambda t, eps, xi, k: 1 / (2 * k) * (np.exp(-t * (1 + eps) * k / xi) - np.exp(-t * (1 - eps) * k / xi))

    Colors = plt.get_cmap("spectral")(np.linspace(0, 0.8, len(ks)))
    RH = 0.28
    d = 2.0
    eta = 82
    eps, xi = 3 / 4 * RH / d, 6 * np.pi * eta * RH

    theoretical11 = partial(theoretical11, eps=eps, xi=xi)
    theoretical12 = partial(theoretical12, eps=eps, xi=xi)

    pp = PdfPages("Plots/{}.pdf".format(Name.replace(" ", "")))
    fig1 = plt.figure()
    ax11 = fig1.add_subplot(2, 1, 1)
    ax12 = fig1.add_subplot(2, 1, 2)
    ax11.set_title("{}".format(Name))

    for k, Color in zip(ks, Colors):
        aY1, aY2, cXY, cY = np.loadtxt("Daten/{}/CrossCorrelation{}.csv".format(Pfad, k), unpack=True) #* 1e3**2
        t1 = np.arange(len(aY1)) * 0.01

        ax11.plot(t1[0:num:steps], cY[0:num:steps], ".", color=Color, label=r"$K = {} \, \frac{{k_{{\mathrm{{B}}}} T}}{{a^2}}: \langle r_{{1, y}}(t') r_{{2, y}}(t' + t) \rangle$".format(k))

        tmaxCorr, maximumCorr = np.array(pd.peakdetect(cY, t1, 10)[0]).T
        tminCorr, minimumCorr = np.array(pd.peakdetect(cY, t1, 10)[1]).T

        if (Pfad == "kext"):
            ax11.plot(t1[0:num:steps/2], theoretical12(t1[0:num:steps/2], k=k), "--", color=Color, linewidth=2)
            ax11.plot(tminCorr[0], minimumCorr[0], "d", color=Color)
            ax11.set_ylim(-0.001, 0.001)

        if (Pfad == "kextFiniteSize"):
            tmaxAuto, maximumAuto = np.array(pd.peakdetect(aY1, t1, 10)[0]).T
            tminAuto, minimumAuto = np.array(pd.peakdetect(aY1, t1, 10)[1]).T
            omega = np.diff(np.sort(np.concatenate((tmaxAuto,tminAuto))))
            print("<Omega> 2 Monomere: ", 2 * np.mean(omega), 2 * np.std(omega))

            ax11.plot(tmaxCorr, maximumCorr, "d", color=Color)
            ax11.plot(tminCorr, minimumCorr, "d", color=Color)

            ax12.plot(tmaxAuto, maximumAuto, "dg", color=Color)
            ax12.plot(tminAuto, minimumAuto, "dg", color=Color)

    k = 1 if (Pfad == "kextFiniteSize") else 50
    aY1, aY2, cXY, cY = np.loadtxt("Daten/{}/CrossCorrelation{}.csv".format(Pfad, k), unpack=True) #* 1e3**2
    ax12.plot(t1[0:num:steps], aY1[0:num:steps], ".", label=r"$\langle r_{{1, y}}(t') r_{{1, y}}(t' + t) \rangle$".format(k))
    ax12.plot(t1[0:num:steps], aY2[0:num:steps], ".", label=r"$\langle r_{{2, y}}(t') r_{{2, y}}(t' + t) \rangle$".format(k))

    if (Pfad == "kextNoHI"):
        D = 0.00139
        tau = 1 / (D * k)
        #aY = np.loadtxt("Daten/OneMonomer/AutoCorrelation{}NoHI.csv".format(k), unpack=True) #* 1e3**2
        #ax12.plot(t1[0:num:steps], aY[0:num:steps], "+", label=r"$\langle r_{{1, y}}(t') r_{{1, y}}(t' + t) \rangle$ for 1 monomer".format(k))
        ax12.plot(t1[0:num:steps], 1 / k * np.exp(-t1[0:num:steps] / tau), "-", linewidth=2.5, color="maroon", label=r"$K = {} \, \frac{{k_{{\mathrm{{B}}}} T}}{{a^2}}:$ theoretical prediction".format(k))
        ax11.set_ylim(-0.0015, 0.0015)
    else:
        if (Pfad == "kext"):
            ax12.plot(t1[0:num:steps/2], theoretical11(t1[0:num:steps/2], k=k), "-", linewidth=2.5, color="maroon", label=r"$K = {} \, \frac{{k_{{\mathrm{{B}}}} T}}{{a^2}}:$ theoretical prediction".format(k))
        elif (Pfad == "kextFiniteSize"):
            aY = np.loadtxt("Daten/OneMonomer/AutoCorrelation{}.csv".format(k), unpack=True) #* 1e3**2
            ax12.plot(t1[0:num:steps], aY[0:num:steps], "+", label=r"$\langle r_{{1, y}}(t') r_{{1, y}}(t' + t) \rangle$ for 1 monomer".format(k))

            tmaxAuto, maximumAuto = np.array(pd.peakdetect(aY, t1, 10)[0]).T
            tminAuto, minimumAuto = np.array(pd.peakdetect(aY, t1, 10)[1]).T
            omega = np.diff(np.sort(np.concatenate((tmaxAuto,tminAuto))))
            print("<Omega> 1 Monomer: ", 2 * np.mean(omega), 2 * np.std(omega))
            ax12.plot(tmaxAuto, maximumAuto, "dr")
            ax12.plot(tminAuto, minimumAuto, "dr")
            ax12.set_ylim(-0.8, 1.5)


    ax11.set_ylabel(r"$\mathrm{CCF} / a^2$")
    lgnd = ax11.legend(loc="best", ncol=2)

    ax11.grid()

    ax12.set_ylabel(r"$\mathrm{ACF} / a^2$")
    lgnd = ax12.legend(loc="best", ncol=2)
    ax12.grid()
    ax12.set_xlim()
    ax12.set_xlabel(r"$t / \sqrt{\frac{ma^2}{k_{\mathrm{B}} T}}$")

    pp.savefig()
    pp.close()

plot(num, steps, ks, "kext", "Monomers with HI")
plot(num, steps, ksNoHI, "kextNoHI", "Monomers without HI")
plot(100000, 1000, ksFiniteSize, "kextFiniteSize", "Monomers with finite-size effects")
