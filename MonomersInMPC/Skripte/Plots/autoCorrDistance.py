import numpy as np
import matplotlib.pyplot as plt
from matplotlib.backends.backend_pdf import PdfPages
import scipy.optimize as sc
from functools import partial

distance = [1.5, 2.0, 2.5]
num = 1000
numFit = 300

theoretical11 = lambda t, eps, xi, k: 1 / (2 * k) * (np.exp(-t * (1 + eps) * k / xi) + np.exp(-t * (1 - eps) * k / xi)) * 1e3**2
theoretical12 = lambda t, eps, xi, k: 1 / (2 * k) * (np.exp(-t * (1 + eps) * k / xi) - np.exp(-t * (1 - eps) * k / xi)) * 1e3**2

k = 1000
RH = 0.28
theoretical12 = partial(theoretical12, k=k)

pp = PdfPages("Plots/CorrelationDistance.pdf")

fig = plt.figure()
ax1 = fig.add_subplot(2, 1, 1)
ax2 = fig.add_subplot(2, 1, 2)
#ax3 = fig.add_subplot(2, 1, 3)

for d in distance:
    eps, xi = 3 / 4 * RH / d, 6 * np.pi * 82 * RH
    print("CrossCorrelation: ", eps, xi)

    aX, aY, aZ, cXY, cX, cY, cZ = np.loadtxt("Daten/distance/CrossCorrelation{:.1f}.csv".format(d), unpack=True) / 1e-3**2
    distanceCorr = np.loadtxt("Daten/distance/AutoCorrelation{}.csv".format(d), unpack=True) / 1e-3**2

    t1 = np.arange(len(cX)) * 0.01

    if (d == 2.0):
        ax1.plot(t1[0:num:10],cXY[0:num:10], "d", label=r"$\langle r_{{1, x}}(t') r_{{2, y}}(t' + t) \rangle$ for $R = {}\, a$".format(d))
        ax1.plot(t1[0:num:5], theoretical12(t1[0:num:5], eps, xi), "maroon", label="theo. prediction for $R = {}\,a$".format(d))
        ax2.plot(t1[0:num:5], theoretical11(t1[0:num:5], eps, xi, k), "maroon", label="theo. prediction for $R = {} \, a$".format(d))
#        ax3.plot(t1[0:num:5], 2 * (aY[0:num:5] - cY[0:num:5]), ".", color="maroon", label=r"$2 (\langle r_{{1, y}}(t') r_{{1, y}}(t' + t) \rangle - \langle r_{{1, y}}(t') r_{{2, y}}(t' + t) \rangle)$ for $R = {}\,a$".format(d))
    ax1.plot(t1[0:num:10], cY[0:num:10], ".", label=r"$\langle r_{{1, y}}(t') r_{{2, y}}(t' + t) \rangle$ for $R = {}\,a$".format(d))
#    ax3.plot(t1[0:num:5], distanceCorr[0:num:5], "+", label=r"simulation for $R = {}\,a$".format(d))
    ax2.plot(t1[0:num:5], aY[0:num:5], ".", label=r"$\langle r_{{1, y}}(t') r_{{1, y}}(t' + t) \rangle$ for $R = {}\, a$".format(d))

_, _, _, _, _, cY, _ = np.loadtxt("Daten/distance/CrossCorrelationNoHI.csv", unpack=True) / 1e-3**2
#ax1.plot(t1[0:num:10],cY[0:num:10], "*", label=r"$\langle r_{{1, y}}(t') r_{{2, y}}(t' + t) \rangle$ without HI".format(d))

#ax3.set_ylabel(r"$\langle \Delta x(t') \Delta x(t' + t) \rangle / (10^{-3} a)^2$")
#ax3.set_xlabel(r"$t / \sqrt{\frac{ma^2}{k_{\mathrm{B}} T}}$")

ax1.set_ylabel(r"$\mathrm{CCF} / (10^{-3} a)^2$")
lgnd = ax1.legend(loc="best", ncol=2)
#ax1.set_ylim(-75, 10)

ax1.grid()

ax2.set_ylabel(r"$\mathrm{ACF} / (10^{-3} a)^2$")
lgnd = ax2.legend(loc="best", ncol=2)
ax2.grid()
ax2.set_xlabel(r"$t / \sqrt{\frac{ma^2}{k_{\mathrm{B}} T}}$")

##ax3.grid()
#lgnd = ax3.legend(loc="best")

pp.savefig()
pp.close()
