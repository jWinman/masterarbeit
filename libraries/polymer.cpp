#include <cmath>
#include <iostream>
#include <libconfig.h++>
#include <omp.h>
#include <random>
#include <sys/stat.h>

#include "polymer.h"

// fancy Konstruktor -------------------------------------------------------------------------
Polymere::Polymere(std::vector<std::mt19937> &generators, libconfig::Config &cfg) :
    timeStep_(cfg.lookup("polymer.timeStepMD")),
    T_(cfg.lookup("simulation.T")),
    polymers_(cfg.lookup("polymer.polymers")), monomers_(cfg.lookup("polymer.monomers")),
    Nx_(cfg.lookup("simulation.Nx")), Ny_(cfg.lookup("simulation.Ny")), Nz_(cfg.lookup("simulation.Nz")),
    wallX_(cfg.lookup("simulation.wallX")), wallY_(cfg.lookup("simulation.wallY")), wallZ_(cfg.lookup("simulation.wallZ")),
    l_(cfg.lookup("polymer.l")),
    k_(cfg.lookup("polymer.k")),
    kappa_(cfg.lookup("polymer.kappa")),
    cut_(pow(2, 1. / 6) * l_),
    eps_(cfg.lookup("polymer.eps")),
    mass_(cfg.lookup("polymer.mass")),
    theta0_(cfg.lookup("polymer.theta0")),
    kext_(cfg.lookup("polymer.kext")),
    start_(2),
    generators_(generators),
    r_(polymers_, std::vector<Eigen::Vector3d>(monomers_)),
    v_(polymers_, std::vector<Eigen::Vector3d>(monomers_)),
    forces_(polymers_, std::vector<std::vector<Eigen::Vector3d>>(monomers_, std::vector<Eigen::Vector3d>(2))),
    boundaries_(polymers_, std::vector<double>(monomers_))
{
    // Deklarierung zum Capturen
    double l = l_;
    double k = k_;
    double eps = eps_;
    double kappa = kappa_;
    double kext = kext_;

    // Kraftefelder
    LJfield_ = [l, eps](Eigen::Vector3d r, double abs_r)
    {
    	double sigma = l;
        return 24 * eps / (sigma * sigma)  * (2 * pow(sigma / abs_r, 14) - pow(sigma / abs_r, 8)) * r;
    };

    springField_ = [k, l](Eigen::Vector3d r, double abs_r)
    {
    	return  k * (abs_r - l) / abs_r * r;
    };

    rigidityField_ = [kappa](Eigen::Vector3d ri, Eigen::Vector3d rj, Eigen::Vector3d rl)
    {
    	return kappa * (ri - 2 * rj + rl);
    };
    
    externSpringField_ = [kext](Eigen::Vector3d r, Eigen::Vector3d rest)
    {
    	return -kext * (r - rest);
    };

    // Potentiale
    LJPot_ = [l, eps](double abs_r)
    {
    	double sigma = l;
    	return 4 * eps * (pow(sigma / abs_r, 12) - pow(sigma / abs_r, 6));
    };

    springPot_ = [k, l](double abs_r)
    {
    	return k / 2. * (abs_r - l) * (abs_r - l);
    };

    // 3 particles interaction
    rigidityPot_ = [kappa](Eigen::Vector3d ri, Eigen::Vector3d rj, Eigen::Vector3d rl)
    {
    	return kappa / 2. * (ri - 2 * rj + rl).squaredNorm();
    };

    start_[0] << cfg.lookup("polymer.rest1.[0]"),
    	      cfg.lookup("polymer.rest1.[1]"),
    	      cfg.lookup("polymer.rest1.[2]");

    start_[1] << cfg.lookup("polymer.rest2.[0]"),
    	      cfg.lookup("polymer.rest2.[1]"),
    	      cfg.lookup("polymer.rest2.[2]");


    initialize();
}

void Polymere::initialize()
{
    // Initialisierung der Poly- / Monomere
    double center_of_massX = 0;
    double center_of_massY = 0;
    double center_of_massZ = 0;
    std::uniform_real_distribution<double> uniform_distribution(0, 1.);
    auto uniform = bind(uniform_distribution, ref(generators_[omp_get_thread_num()]));
    double theta = (90 - theta0_) * 2. * M_PI / 360.;
    double phi = 0 * 2 * M_PI;

    for (int p = 0; p < polymers_; p++)
    {
	for (int m = 0; m < monomers_; m++)
	{
    	    if (m == 0)
    	    {
    	        r_[p][m] = (p == 0) ? start_[0] : start_[1];

    	    }
    	    else if (m % 2 == 0)
    	    {
    	        r_[p][m][0] = r_[p][m - 1][0] + l_ * sin(M_PI - theta) * cos(phi);
    	        r_[p][m][1] = r_[p][m - 1][1] + l_ * sin(M_PI - theta) * sin(phi);
    	        r_[p][m][2] = r_[p][m - 1][2] + l_ * cos(M_PI - theta);
    	    }
    	    else
    	    {
    	        r_[p][m][0] = r_[p][m - 1][0] + l_ * sin(theta) * cos(phi);
    	        r_[p][m][1] = r_[p][m - 1][1] + l_ * sin(theta) * sin(phi);
    	        r_[p][m][2] = r_[p][m - 1][2] + l_ * cos(theta);
    	    }

    	    v_[p][m][0] = uniform();
    	    v_[p][m][1] = uniform();
    	    v_[p][m][2] = uniform();

    	    boundaries_[p][m] = 0;
	}
    }

    #pragma omp parallel
    {
    	#pragma omp for reduction(+:center_of_massX, center_of_massY, center_of_massZ) collapse(2)
    	for (int p = 0; p < polymers_; p++)
    	{
    	    for (int m = 0; m < monomers_; m++)
    	    {
    	    	center_of_massX += v_[p][m][0] / (polymers_ * monomers_);
    	    	center_of_massY += v_[p][m][1] / (polymers_ * monomers_);
    	    	center_of_massZ += v_[p][m][2] / (polymers_ * monomers_);
    	    }
    	}

	#pragma omp for collapse(2)
	for (int p = 0; p < polymers_; p++)
	{
	    for (int m = 0; m < monomers_; m++)
	    {
		v_[p][m][0] -= center_of_massX;
		v_[p][m][1] -= center_of_massY;
		v_[p][m][2] -= center_of_massZ;
	    }
	}
    }
}

// Berechnung aller Kräfte -------------------------------------------------------------
void Polymere::calcForces()
{
    // neue Kräfte werden alte Kräfte
    // neue Kräfte = 0 setzen
    #pragma omp parallel for collapse(2)
    for (int p = 0; p < polymers_; p++)
    {
    	for (int m = 0; m < monomers_; m++)
    	{
    	    forces_[p][m][0] = forces_[p][m][1];
    	    forces_[p][m][1] = Eigen::Vector3d::Zero();
    	}
    }

    for (int i = 0; i < (polymers_ * monomers_); i++)
    {
    	for (int j = 0; j < i; j++)
    	{
    	    int pbar = i / monomers_;
    	    int mbar = i % monomers_;
    	    int p = j / monomers_;
    	    int m = j % monomers_;

	    Eigen::Vector3d diffLJ = boundary(r_[pbar][mbar] - r_[p][m]);
	    double abs_rLJ = diffLJ.norm();

	    if (abs_rLJ < cut_)
	    {
		forces_[pbar][mbar][1] += LJfield_(diffLJ, abs_rLJ);
		forces_[p][m][1] -= LJfield_(diffLJ, abs_rLJ);
	    }
    	}
    }

    Eigen::Vector3d rest = Eigen::Vector3d::Zero();
    double theta = (90 - theta0_) * 2. * M_PI / 360.;
    double phi = 0;
    for (int p = 0; p < polymers_; p++)
    {
    	for (int m = 0; m < monomers_; m++)
    	{
	    if (m == 0)
	    {
		rest = (p == 0) ? start_[0] : start_[1];
	    }
	    else if (m % 2 == 0)
	    {
		rest[0] = rest[0] + l_ * sin(M_PI - theta) * cos(phi);
		rest[1] = rest[1] + l_ * sin(M_PI - theta) * sin(phi);
		rest[2] = rest[2] + l_ * cos(M_PI - theta);
	    }
	    else
	    {
		rest[0] = rest[0] + l_ * sin(theta) * cos(phi);
		rest[1] = rest[1] + l_ * sin(theta) * sin(phi);
		rest[2] = rest[2] + l_ * cos(theta);
	    }
	    forces_[p][m][1] += externSpringField_(r_[p][m], rest);

    	    if (m < monomers_ - 1)
    	    {
		Eigen::Vector3d diff = r_[p][m] - r_[p][m + 1];
		double abs_r = diff.norm();

		forces_[p][m][1] -= springField_(diff, abs_r);
		forces_[p][m + 1][1] += springField_(diff, abs_r);
	    }
    	    if (m < monomers_ - 2)
    	    {
    	    	forces_[p][m][1] -= rigidityField_(r_[p][m], r_[p][m + 1], r_[p][m + 2]);
    	    	forces_[p][m + 1][1] += 2 * rigidityField_(r_[p][m], r_[p][m + 1], r_[p][m + 2]);
    	    	forces_[p][m + 2][1] -= rigidityField_(r_[p][m], r_[p][m + 1], r_[p][m + 2]);
    	    }
    	}
    }
}

// period. Randbedingungen ---------------------------------------------------------------------
Eigen::Vector3d Polymere::boundary(Eigen::Vector3d r)
{
    if (!wallX_)
    {
	r[0] -= Nx_ * floor(r[0] / Nx_ + 0.5);
    }
    if (!wallY_)
    {
	r[1] -= Ny_ * floor(r[1] / Ny_ + 0.5);
    }
    if (!wallZ_)
    {
	r[2] -= Nz_ * floor(r[2] / Nz_ + 0.5);
    }

    return r;
}

// Geschwindigkeits-Verlet-Algorithmus -------------------------------------------------
void Polymere::Verlet()
{
    for (int p = 0; p < polymers_; p++)
    {
    	for (int m = 0; m < monomers_; m++)
    	{
    	    r_[p][m] += v_[p][m] * timeStep_ + 1. / (2 * mass_) * forces_[p][m][1] * timeStep_ * timeStep_;
    	    noSlip(p, m);
    	}
    }
    calcForces();

    for (int p = 0; p < polymers_; p++)
    {
    	for (int m = 0; m < monomers_; m++)
    	{
    	    v_[p][m] += 1. / (2 * mass_) * (forces_[p][m][0] + forces_[p][m][1]) * timeStep_;
    	    bounce_backVel(p, m);
    	}
    }
}

// bounce back boundary ----------------------------------------------------------------
// Deltah ------------------------------------------------------------------------------
void Polymere::noSlip(int p, int m)
{
    std::array<bool, 3> borders = {wallX_ && (r_[p][m][0] < 0 || r_[p][m][0] > Nx_),
    	                           wallY_ && (r_[p][m][1] < 0 || r_[p][m][1] > Ny_),
    	                           wallZ_ && (r_[p][m][2] < 0 || r_[p][m][2] > Nz_)};
    int wallHits = std::count_if(borders.begin(), borders.end(), [](bool x){return x;});
    if (wallHits == 0)
    {
    	return;
    }
    double deltar[3] = {r_[p][m][0] - Nx_ * (r_[p][m][0] - Nx_ >= 0),
    	                r_[p][m][1] - Ny_ * (r_[p][m][1] - Ny_ >= 0),
    	                r_[p][m][2] - Nz_ * (r_[p][m][2] - Nz_ >= 0)};

    double deltah[3] = {h(v_[p][m][0], forces_[p][m][1][0] / mass_, deltar[0], borders[0]),
                        h(v_[p][m][1], forces_[p][m][1][1] / mass_, deltar[1], borders[1]),
                        h(v_[p][m][2], forces_[p][m][1][2] / mass_, deltar[2], borders[2])};
    if (wallHits == 1)
    {
    	if (borders[0])
    	{
    	    bounce_backPos(p, m, deltah[0]);
    	}
    	else if(borders[1])
    	{
    	    bounce_backPos(p, m, deltah[1]);
    	}
    	else
    	{
    	    bounce_backPos(p, m, deltah[2]);
    	}
    }
    else
    {
	//bubble sort
    	for (int i = 3; i > 0; i--)
    	{
    	    for (int j = 0; j < i - 1; j++)
    	    {
    	    	if (deltah[j] > deltah[j + 1])
    	    	{
    	    	    double tmp_deltah = deltah[j];
    	    	    deltah[j] = deltah[j + 1];
    	    	    deltah[j + 1] = tmp_deltah;

    	    	    bool tmp_border = borders[j];
    	    	    borders[j] = borders[j + 1];
    	    	    borders[j + 1] = tmp_border;
    	    	}
    	    }
    	}

	int comp = 2;
	while (!borders[comp] && comp >= 0)
	{
	    comp--;
	}
	bounce_backPos(p, m, deltah[comp]);
    }
}

void Polymere::bounce_backPos(int p, int m, double deltah)
{
    r_[p][m] += v_[p][m] * (-2 * deltah) + 2 * forces_[p][m][1] / mass_ * deltah * deltah;
    boundaries_[p][m] = deltah;
}

void Polymere::bounce_backVel(int p, int m)
{
    double deltah = boundaries_[p][m];
    if (deltah > 0)
    {
    	v_[p][m] = -v_[p][m] + 2 * (forces_[p][m][0] + forces_[p][m][1]) / (2 * mass_) * deltah;
    	boundaries_[p][m] = 0;
    }
}

double Polymere::h(double v, double a, double r, bool wall)
{
    if (wall == false)
    {
    	return 0;
    }
    if (std::fabs(a) == 0)
    {
    	return r / v;
    }
    if (a > 0 && r < 0)
    {
    	return - v / a - sqrt(v * v / (a * a) + 2 * r / a);
    }
    if (a < 0 && r < 0)
    {
    	return - v / a + sqrt(v * v / (a * a) + 2 * r / a);
    }
    if (a > 0 && r > 0)
    {
    	return - v / a + sqrt(v * v / (a * a) + 2 * r / a);
    }
    if (a < 0 && r > 0)
    {
    	return - v / a - sqrt(v * v / (a * a) + 2 * r / a);
    }
    return 0.;
}

//  Ausgabe ----------------------------------------------------------------------------
void Polymere::OutputPos(int t)
{
    char positions[100];
    if (t == 0)
    {
    	mkdir("Daten/Animation", 0777);
    }
    sprintf(positions, "Daten/Animation/positions-%d.csv", t);
    FILE *fileP = fopen(positions, "w");
    for (int p = 0; p < polymers_; p++)
    {
	for (int m = 0; m < monomers_; m++)
	{
	    fprintf(fileP, "%.22f %.22f %.22f\n", r_[p][m][0], r_[p][m][1], r_[p][m][2]);
	}
    }
    fclose(fileP);
}

// kinetische Energie ----------------------------------------------------------------------
double Polymere::Ekin()
{
    double Ekin = 0;
    #pragma omp parallel for collapse(2) reduction(+:Ekin)
    for (int p = 0; p < polymers_; p++)
    {
    	for (int m = 0; m < monomers_; m++)
    	{
	    Ekin += v_[p][m].squaredNorm();
    	}
    }
    return 0.5 * mass_ * Ekin / (polymers_ * monomers_);
}

// potentielle Energie ---------------------------------------------------------------------
double Polymere::EPot()
{
    double Epot = 0;
    for (int i = 0; i < (polymers_ * monomers_); i++)
    {
    	for (int j = 0; j < i; j++)
    	{
    	    int pbar = i / monomers_;
    	    int mbar = i % monomers_;
    	    int p = j / monomers_;
    	    int m = j % monomers_;

	    Eigen::Vector3d diffLJ = boundary(r_[p][m] - r_[pbar][mbar]);
	    double abs_rLJ = diffLJ.norm();

	    if (abs_rLJ < cut_)
	    {
		Epot += LJPot_(abs_rLJ) + eps_;
	    }
    	}
    }

    for (int p = 0; p < polymers_; p++)
    {
    	for (int m = 0; m < monomers_ - 1; m++)
    	{
	    Eigen::Vector3d diff = r_[p][m + 1] - r_[p][m];
	    double abs_r = diff.norm();

	    Epot += springPot_(abs_r);
	    if (m < monomers_ - 2)
	    {
		Epot += rigidityPot_(r_[p][m], r_[p][m + 1], r_[p][m + 2]);
	    }
    	}
    }
    return Epot / (polymers_ * monomers_);
}

// Tangentenkorrelationsfunktion ------------------------------------------------------------
void Polymere::tangentialKorr(int measurements, int p, int bond0, std::vector<double> &tangKorr, int D)
{
    std::vector<Eigen::Vector3d> bonds(monomers_ - 1);
    for (unsigned int m = 0; m < bonds.size(); m++)
    {
    	bonds[m] = (r_[p][m] - r_[p][m + 1]);
    	if (D == 2)
    	{
    	    bonds[m][2] = 0;
    	}
    	bonds[m].normalize();
    }

    for (unsigned int m = 0; m < bonds.size() - bond0; m++)
    {
    	for (unsigned int n = 0; n < bonds.size() - bond0 - m; n++)
    	{
    	    tangKorr[m] += bonds[n + bond0].dot(bonds[m + n + bond0]) / ((bonds.size() - bond0 - m) * measurements);
    	}
    }
}

// Autokorrelationsfunktion -----------------------------------------------------------------
Eigen::VectorXd Polymere::CrossCorr(int monomer1, int monomer2)
{
    Eigen::VectorXd tuple(6);
    if (polymers_ < 2)
    {
    	tuple << r_[0][monomer1], Eigen::Vector3d::Zero();
    	return tuple;
    }
    else
    {
    	tuple << r_[0][monomer1], r_[1][monomer2];
    	return tuple;
    }
}

double Polymere::distance(int monomer1, int monomer2)
{
    Eigen::Vector3d dist = Eigen::Vector3d::Zero();
    dist = (polymers_ < 2) ? r_[0][monomer1] : (r_[1][monomer1] - r_[0][monomer2]);
    return dist[1];
}

// Center of Mass des Polymers --------------------------------------------------------------
Eigen::Vector3d Polymere::CenterOfMass(int p)
{
    Eigen::Vector3d center = Eigen::Vector3d::Zero();
    for (int m = 0; m < monomers_; m++)
    {
    	center += r_[p][m] / monomers_;
    }
    return center;
}

Eigen::Vector3d Polymere::CenterOfMassVel(int p)
{
    Eigen::Vector3d cmsVel = Eigen::Vector3d::Zero();
    for (int m = 0; m < monomers_; m++)
    {
    	cmsVel += v_[p][m] / monomers_;
    }
    return cmsVel;
}

// Set-Geschwindigkeiten --------------------------------------------------------------------
void Polymere::SetVel(std::vector<Eigen::Vector3d> v)
{
    for (int p = 0; p < polymers_; p++)
    {
    	for (int m = 0; m < monomers_; m++)
    	{
    	    v_[p][m] = v[p * monomers_ + m];
    	}
    }
}

// Set & Get-Geschwindigkeiten & Positionen --------------------------------------------------
std::tuple<std::vector<Eigen::Vector3d>, std::vector<Eigen::Vector3d>> Polymere::GetPosVel()
{
    std::vector<Eigen::Vector3d> r(polymers_ * monomers_);
    std::vector<Eigen::Vector3d> v(polymers_ * monomers_);
    for (int p = 0; p < polymers_; p++)
    {
    	for (int m = 0; m < monomers_; m++)
    	{
    	    r[p * monomers_ + m] = r_[p][m];
    	    v[p * monomers_ + m] = v_[p][m];
    	}
    }
    return std::make_tuple(r, v);
}
