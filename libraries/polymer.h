#include <Eigen/Dense>
#include <functional>
#include <libconfig.h++>
#include <vector>

#ifndef POLYMERE_H
#define POLYMERE_H

class Polymere
{
    private:
	double timeStep_; // Zeitschritt für MD
	double T_; // Temperatur
	int polymers_, monomers_;  // Anzahl / Länge der Polymere
	int Nx_, Ny_, Nz_; // Abmessungen
	bool wallX_, wallY_, wallZ_; // Wände
	double l_; // bond length
	double k_; // spring constant
	double kappa_; // bending
	double cut_; // cut-off radius
	double eps_;
	double mass_;
	double theta0_;
	double kext_;
	std::vector<Eigen::Vector3d> start_;
	std::vector<std::mt19937> generators_;

	// Kraftfelder
	std::function<Eigen::Vector3d(Eigen::Vector3d, double)> LJfield_; // Lennard-Jones Kraftfeld
	std::function<Eigen::Vector3d(Eigen::Vector3d, double)> springField_; // Kraftfeld der Feder
	std::function<Eigen::Vector3d(Eigen::Vector3d, Eigen::Vector3d, Eigen::Vector3d)> rigidityField_; // Bending potential
	std::function<Eigen::Vector3d(Eigen::Vector3d r, Eigen::Vector3d rest)> externSpringField_;

	std::vector<std::vector<Eigen::Vector3d>> r_; // Pos. der Polymere/Monomere
	std::vector<std::vector<Eigen::Vector3d>> v_; // Geschw. der Polymere/Monomere
	std::vector<std::vector<std::vector<Eigen::Vector3d>>> forces_; // Kräfte des jetzigen und vorherigen Zeitschritts
	std::vector<std::vector<double>> boundaries_;


	// Potentiale
	std::function<double(double)> LJPot_;
	std::function<double(double)> springPot_;
	std::function<double(Eigen::Vector3d ri, Eigen::Vector3d rj, Eigen::Vector3d rl)> rigidityPot_;

    public:
    	Polymere(std::vector<std::mt19937> &generators, libconfig::Config &cfg);
    	void initialize();
    	void calcForces();
	Eigen::Vector3d boundary(Eigen::Vector3d r);
    	void Verlet();
    	void bounce_backPosTimeStep(int p, int m);
    	void bounce_backVelTimeStep(int p, int m);
    	void noSlipTimeStep(int p, int m);
    	double h(double v, double a, double r, bool wall);
    	void bounce_backPos(int p, int m, double deltah);
    	void bounce_backVel(int p, int m);
    	void noSlip(int p, int m);
    	void OutputPos(int t);
    	double Ekin();
    	double EPot();
	Eigen::Vector3d CenterOfMass(int p);
	Eigen::Vector3d CenterOfMassVel(int p);

    	void SetVel(std::vector<Eigen::Vector3d> v);
	std::tuple<std::vector<Eigen::Vector3d>, std::vector<Eigen::Vector3d>> GetPosVel();
	void tangentialKorr(int measurements, int p, int bond0, std::vector<double> &tangKorr, int D=3);
	Eigen::VectorXd CrossCorr(int monomer1, int monomer2);
	double distance(int monomer1, int monomer2);
    	~Polymere(){}
};
#endif
