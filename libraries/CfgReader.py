import ast

class CfgReader:
    contentStr = ""
    dictionair = {}

    def __init__(self, fileName):
        """
        Konstruktor open and reads fileName line by line.
        Comments starting with "//" will be ignored.
        Lines outside of a group get a "'" at the beginning and "," at the end.
        All groups are turned into a dictionairy-like string, contentStr.
        contentStr is then turned into dictioniar.
        """

        with open(fileName) as f:
            for line in f.readlines():
                if (line[:2] == "//" or line[0] == "\n"):
                    continue
                elif (line[0] == "{" or line[0] == "}"):
                    self.contentStr += line
                else:
                    self.contentStr += "'" + line

        # chars for creating a dict
        oldChars = ["\n", ":", " =", ";", ",},", " ", "false", "true"]
        newChars = ["",  "':", "':", ",", "}, ",  "", "False", "True"]

        for oldChar, newChar in zip(oldChars, newChars):
            self.contentStr = self.contentStr.replace(oldChar, newChar)
        self.contentStr = "{" + self.contentStr[:-1] + "}"

        self.dictionair = ast.literal_eval(self.contentStr)

    def lookup(self, string):
        """
        lookup(string) looks up the path in the opened fileName.
        """

        value = self.dictionair
        for Str in string.split("."):
            value = value[Str]

        return value
