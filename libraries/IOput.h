#include <Eigen/Dense>
#include <libconfig.h++>
#include <string>
#include <vector>

#ifndef IOPUT_H
#define IOPUT_H

class IOput
{
    private:
	int var_;
	std::string varName_;

    public:
    	IOput(libconfig::Config &cfg, int argc, std::string var, std::string varName);
    	void setParams(libconfig::Config &cfg, int argc, char* argv[]);
    	void writeConfig(libconfig::Config &cfg);
    	void print(std::string observable,
		   std::vector<Eigen::Vector3d> array);
    	void print(std::string observable,
		   std::vector<Eigen::VectorXd> array);
	void print(std::string observable,
		   std::vector<double> array1, std::vector<double> array2);
    	void print(std::string observable,
		   std::vector<double> array);
	void printVelTopView(std::vector<std::vector<Eigen::Vector3d>> meanVcm, int Nx, int Ny);
	void printVelSideView(std::vector<std::vector<Eigen::Vector3d>> meanVcm, int Ny, int Nz);
	void printMeanVel3d(std::vector<std::vector<std::vector<Eigen::Vector3d>>> meanVcm, int Nx, int Ny, int Nz);
	void AppendToFile(std::string observable,
		          std::vector<Eigen::Vector3d> array, int t);
	void AppendToFile(std::string observable,
		          std::vector<double> array, int t);

    	~IOput(){};
};

#endif
