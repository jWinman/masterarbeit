#include <fstream>
#include <iostream>
#include <iomanip>
#include <libconfig.h++>
#include <string>
#include <vector>

#include "IOput.h"

IOput::IOput(libconfig::Config &cfg, int argc, std::string var, std::string varName)
{
    if (argc == 1)
    {
    	var_ = 0;
    	varName_ = "Daten";
    }
    else
    {
    	var_ = std::stoi(var);
    	varName_ = varName;
    }
}

void IOput::setParams(libconfig::Config &cfg, int argc, char* argv[])
{
    for (int i = 1; i < argc - 1; i += 2)
    {
	libconfig::Setting &variable = cfg.lookup(argv[i]);
	bool strTrue = std::strcmp(argv[i + 1], "true") == 0;
	bool strFalse = std::strcmp(argv[i + 1], "false") == 0;
	bool strBool = strTrue or strFalse;

    	if (strBool)
    	{
    	    variable = strTrue;
    	}
    	else
    	{
	    std::string argvStr = argv[i + 1];
	    bool point = argvStr.find_first_of(".") != std::string::npos;
	    bool expon = argvStr.find_first_of("e") != std::string::npos;
	    if (point or expon)
	    {
	    	variable = std::stod(argv[i + 1]);
	    }
	    else
	    {
	    	variable = std::stoi(argv[i + 1]);
	    }
    	}
    }
}

void IOput::writeConfig(libconfig::Config &cfg)
{
    char path[100];
    sprintf(path, "%s/parameter%d.cfg", varName_.c_str(), var_);
    cfg.writeFile(path);
}

void IOput::print(std::string observable, std::vector<Eigen::Vector3d> array)
{
    printf("Output: %s \n", observable.c_str());
    char path[100];
    sprintf(path, "%s/%s%d.csv", varName_.c_str(), observable.c_str(), var_);
    FILE *file = fopen(path, "w");
    for (unsigned int t = 0; t < array.size(); t++)
    {
        fprintf(file, "%.15f %.15f %.15f\n", array[t][0], array[t][1], array[t][2]);
    }
    fclose(file);
}

void IOput::print(std::string observable, std::vector<Eigen::VectorXd> array)
{
    printf("Output: %s \n", observable.c_str());
    std::string path = varName_ + "/" + observable + std::to_string(var_) + ".csv";

    std::ofstream file;
    file.open(path);
    for (unsigned int t = 0; t < array.size(); t++)
    {
    	file << std::setprecision(15) << array[t].transpose() << std::endl;
    }
    file.close();
}

void IOput::print(std::string observable, std::vector<double> array)
{
    printf("Output: %s \n", observable.c_str());
    char path[100];
    sprintf(path, "%s/%s%d.csv", varName_.c_str(), observable.c_str(), var_);
    FILE *file = fopen(path, "w");
    for (unsigned int t = 0; t < array.size(); t++)
    {
        fprintf(file, "%d %.15f\n", t, array[t]);
    }
    fclose(file);
}

void IOput::print(std::string observable, std::vector<double> array1, std::vector<double> array2)
{
    printf("Output: %s \n", observable.c_str());
    char path[100];
    sprintf(path, "%s/%s%d.csv", varName_.c_str(), observable.c_str(), var_);
    FILE *file = fopen(path, "w");
    	for (unsigned int t = 0; t < array1.size(); t++)
    	{
	    fprintf(file, "%d %.15f %.15f %.15f\n", t, array1[t], array2[t], array1[t] + array2[t]);
    	}
    fclose(file);
}

void IOput::printVelTopView(std::vector<std::vector<Eigen::Vector3d>> meanVcm, int Nx, int Ny)
{
    printf("Output: VelTopView\n");
    std::string mean2d = varName_ + "/VelTopView.csv";
    FILE *fileMean = fopen(mean2d.c_str(), "w");
    for (int x = 0; x < Nx; x++)
    {
	for (int y = 0; y < Ny; y++)
	{
	    fprintf(fileMean, "%d %d %.15f %.15f \n", x, y, meanVcm[x][y][0], meanVcm[x][y][1]);
	}
    }
    fclose(fileMean);
}

void IOput::printVelSideView(std::vector<std::vector<Eigen::Vector3d>> meanVcm, int Ny, int Nz)
{
    printf("Output: VelSideView\n");
    std::string mean2d = varName_ + "/VelSideView.csv";
    FILE *fileMean = fopen(mean2d.c_str(), "w");
    for (int y = 0; y < Ny; y++)
    {
	for (int z = 0; z < Nz; z++)
	{
	    fprintf(fileMean, "%d %d %.15f %.15f %.15f \n", y, z, meanVcm[y][z][0], meanVcm[y][z][1], meanVcm[y][z][2]);
	}
    }
    fclose(fileMean);
}

void IOput::printMeanVel3d(std::vector<std::vector<std::vector<Eigen::Vector3d>>> meanVcm3d, int Nx, int Ny, int Nz)
{
    printf("Output: Vel3d\n");
    std::string mean3d = varName_ + "/mean3d.csv";
    FILE *fileMean = fopen(mean3d.c_str(), "w");
    for (int x = 0; x < Nx; x++)
    {
	for (int y = 0; y < Ny; y++)
	{
	    for (int z = 0; z < Nz; z++)
	    {
		fprintf(fileMean, "%d %d %d %.15f %.15f %.15f\n", x, y, z, meanVcm3d[x][y][z][0], meanVcm3d[x][y][z][1], meanVcm3d[x][y][z][2]);
	    }
	}
    }
    fclose(fileMean);
}

void IOput::AppendToFile(std::string observable, std::vector<Eigen::Vector3d> array, int t)
{
    std::string path = varName_ + "/" + observable + ".csv";
    FILE *file = (t == 0) ? fopen(path.c_str(), "w") : fopen(path.c_str(), "a");
    for (unsigned int i = 0; i < array.size(); i++)
    {
    	fprintf(file, "%.15f %.15f %.15f \n", array[i][0], array[i][1], array[i][2]);
    }
    fclose(file);
}

void IOput::AppendToFile(std::string observable, std::vector<double> array, int t)
{
    std::string path = varName_ + "/" + observable + ".csv";
    FILE *file = (t == 0) ? fopen(path.c_str(), "w") : fopen(path.c_str(), "a");
    for (unsigned int i = 0; i < array.size(); i++)
    {
    	fprintf(file, "%d %.15f\n", t, array[i]);
    }
    fclose(file);
}
