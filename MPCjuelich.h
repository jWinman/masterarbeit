#ifndef MPC_H
#define MPC_H

#include <iostream>
#include <cstdlib>
#include <iomanip>
#include <stdint.h>
#include <fstream>
#include <cmath>
#include <vector>
#include <ctime>       ///für Zufallszahlen (seed)

using namespace std;

///---------------------///
///Kontrollstrukturen://///
#define  Thermostat	0	/// 1 = Thermostat ON; 	   0 = Thermostat OFF
//#define  BounCon	0 	/// 1 = NoSlip; 	   0 = Periodic Boun
#define  ShearVel	0	/// 1 = Shear Velocity ON; 0 = Shear Velocity OFF
///---------------------///
///-///////////////////-///

#ifndef  CELL_SIZE
#define  CELL_SIZE      1 ///Zellgröße (im Skript a)
#endif

#ifndef  MASS_SOLVENT
#define  MASS_SOLVENT        1   ///Masse der fluidpartiklel
#endif

#ifndef  k_BT
#define  k_BT           1
#endif

#ifndef  RANDOM_SHIFT
#define  RANDOM_SHIFT   CELL_SIZE/2.0 ///Random shift für Galilei-Invarianz
#endif

#ifndef  NCELLSX
#define  NCELLSX         10 ///Anzahl der Zellen in x-Richtung
#endif

#ifndef  NCELLSY
#define  NCELLSY         10 ///Anzahl der Zellen in y-Richtung
#endif

#ifndef  NCELLSZ
#define  NCELLSZ         10 ///Anzahl der Zellen in z-Richtung
#endif

#ifndef  BOXLX
#define  BOXLX          NCELLSX * CELL_SIZE ///Länge der Box in x-Richtung
#endif

#ifndef  BOXLY
#define  BOXLY          NCELLSY * CELL_SIZE ///Länge der Box in y-Richtung
#endif

#ifndef  BOXLZ
#define  BOXLZ          NCELLSZ * CELL_SIZE ///Länge der Box in z-Richtung
#endif

#ifndef  NCELLS
#define  NCELLS          (NCELLSX * NCELLSY * NCELLSZ) ///Gesamtanzahl der Zellen
#endif

#ifndef  N_SOLVENT_PER_CELL
#define  N_SOLVENT_PER_CELL     10  ///Teilchen pro Zelle
#endif

#ifndef	 N_SOLVENT_REAL
#define  N_SOLVENT_REAL          NCELLSX * NCELLSY * NCELLSZ * N_SOLVENT_PER_CELL ///Gesamtanzahl der Teilchen
#endif

#ifndef  DELTA_T_MPC
#define  DELTA_T_MPC        0.1 ///Zeitschritt
#endif

#ifndef  ROT_ANGLE
#define  ROT_ANGLE      (130.0 / 180.0 * M_PI) ///Rotationswinkel (im Skript alpha)
#endif

#define  FACTOR_BINS		2 ///Faktor, um den die Anzahl der Zellen vervielfacht werden soll
#define  NCELLS_BINX      NCELLSX * FACTOR_BINS
#define  NCELLS_BINY      NCELLSY * FACTOR_BINS
#define  NCELLS_BINZ      NCELLSZ * FACTOR_BINS

#ifndef	 FREQ_SAMP
#define  FREQ_SAMP	10
#endif

/*
#ifndef  N_MONO_PER_CHAIN
#define  N_MONO_PER_CHAIN	10      ///Anzahl der Teilchen pro Kette
#endif

#ifndef  N_CHAIN
#define  N_CHAIN	2       ///Anzahl der Ketten
#endif

#ifndef	 N_MONO_TOT
#define  N_MONO_TOT		    N_CHAIN * N_MONO_PER_CHAIN
#endif

#ifndef   MASS_MONO
#define  MASS_MONO       	N_SOLVENT_PER_CELL * MASS_SOLVENT
#endif


#ifndef  CYCLES_MPC
#define  CYCLES_MPC     20000
#endif

#ifndef  NTOT
#define  NTOT               (N_SOLVENT_REAL + N_MONO_TOT)
#endif

#ifndef  DEL_T_MD
#define  DEL_T_MD		0.005
#endif

#ifndef  CYCLE_VERLET
#define  CYCLE_VERLET    	20 /// = 0.1/0.005
#endif
*/


namespace {

  ///für die Ausgabe:
  FILE *Energy;
  FILE *LinMom;
  
  int N_PBC[NTOT][3] = {0};
//  double FORCE_MD[N_MONO_TOT][3] = {0.0};
  double Pos[NTOT][3] = {0.0}; ///Positions-Vektor für alle Teilchen
  double Vel[NTOT][3] = {0.0}; ///Geschwindigkeits-Vektor für alle Teilchen
  
  //double newPos[NTOT][3] = {0.0}; ///Hilfsarray zur Berechnug des Schwerpunkts in den Zellen
  //double rrel[NTOT][3]={0.0}; ///Relativkoordinaten der Teilchen
  //double vrel[NTOT][3]={0.0}; ///Relativgeschwindigkeit der Teilchen
  int    Cell[NTOT] = {0}; /// Zellenindex aller Teilchen
  
  int Mass[NCELLS] = {0};
  double Orig[3] = {0.0}; ///Deklaration des Galilei-Invarianz-shifts
  int NperCELL[NCELLS] = {0}; ///Teilchen in der Zelle

  double Vcm[NCELLS][3] = {0.0}; ///CM-velocities of each cell
  double Ek[NCELLS] = {0.0}; ///kinetic energy of each cell

  //double I[NCELLS][3][3]; ///inertia tensor
  //double DET[NCELLS] = {0.0}; ///determinant of the inertia tensor
  //double invI[NCELLS][3][3]; ///inverse inertia tensor

  double fac[NCELLS] = {0.0}; ///FACTOR_BINS of canonical thermostat
  //double deltaL[NCELLS][3] = {0.0}; ///Drehimpulsaenderung vor-nach collision
  //double delOMEGA[NCELLS][3] = {0.0}; ///Winkelgeschwindigkeitsaenderung vor-nach collision

  double D_rot[NCELLS][3][3]; ///rotation Matrix

  const double c = cos(ROT_ANGLE); ///Wichtig für Matrix D_rot
  const double s = sin(ROT_ANGLE); ///Wichtig für Matrix D_rot
  double phi, theta; ///Winkel mit Zufallszahlen --> Wichtig für Matrix D_rot
  double R_x, R_y, R_z; ///Wichtig für Matrix D_rot
  
   double Pbefore[NCELLS][3] = {0.0}; ///Ausgabe Impuls nach dem Stoss
   double Pafter[NCELLS][3] = {0.0}; ///Ausgabe Impuls nach dem Stoss
}

///Deklaration und Definitionen der Funktionen:
///Zufallszahlengenerator:

///Gammaverteilung:
double rgamma(int Nc) {
  const double d = 1.5 * Nc - 1.833333333333333333333333333333333333333333333333333333333333333; // 3 (Nc - 1) / 2 - 1 / 3
  const double c = 0.33333333333333333333333333333333333333333333333333333333 / sqrt(d); // (1.0 / 3.0) / sqrt(d);
  double x, v;
  for (; ;) {
    do {
      x = ran_gauss_ziggurat ();
      v = 1.0 + c * x;
    } while (v <= 0.0);
    v = v * v * v;
    double u = genrand_real1(), x2 = x * x, x4 = x2 * x2;
    if (u < 1.0 - 0.0331 * x4) return d * v;
    if (log (u) < 0.5 * x2 + d * (1.0 - v + log (v))) return d * v;
  }
} // gamma distributed relative kinetic energy

///Routine to calculate the lin momentum in each cell before the collision:
void calcLinMomBefore() {

  for (size_t n = 0; n < N_SOLVENT_REAL; ++n) { ///alle Teilchen durchgehen
        int idx = Cell[n]; ///Zelle des Teilchens n wird aus dem Vektor Cell[NCELLS] entnommen

	Pbefore[idx][0] += Vel[n][0];
	Pbefore[idx][1] += Vel[n][1];
	Pbefore[idx][2] += Vel[n][2];
  }
  for (size_t n = N_SOLVENT_REAL; n < NTOT; ++n) { ///alle Teilchen durchgehen
        int idx = Cell[n]; ///Zelle des Teilchens n wird aus dem Vektor Cell[NCELLS] entnommen

	Pbefore[idx][0] += (Vel[n][0]) * MASS_MONO;
	Pbefore[idx][1] += (Vel[n][1]) * MASS_MONO;
	Pbefore[idx][2] += (Vel[n][2]) * MASS_MONO;
  }
}

///Routine to calculate the lin momentum in each cell after the collision:
void calcLinMomAfter() {

   for (size_t n = 0; n < N_SOLVENT_REAL; ++n) { ///alle Teilchen durchgehen
        int idx = Cell[n]; ///Zelle des Teilchens n wird aus dem Vektor Cell[NCELLS] entnommen

	Pafter[idx][0] += Vel[n][0];
	Pafter[idx][1] += Vel[n][1];
	Pafter[idx][2] += Vel[n][2];
  }
  for (size_t n = N_SOLVENT_REAL; n < NTOT; ++n) { ///alle Teilchen durchgehen
        int idx = Cell[n]; ///Zelle des Teilchens n wird aus dem Vektor Cell[NCELLS] entnommen

	Pafter[idx][0] += (Vel[n][0]) * MASS_MONO;
	Pafter[idx][1] += (Vel[n][1]) * MASS_MONO;
	Pafter[idx][2] += (Vel[n][2]) * MASS_MONO;
  }
}

///Routine to calculate the VDF
void calcVDF(int OPT) {

  static const int NHIST = 300;
  static vector< size_t > Hx(NHIST, 0);
  static vector< size_t > Hy(NHIST, 0);
  static vector< size_t > Hz(NHIST, 0);
const double MIN = -5.0;
const double MAX = 5.0;
const double INTERVALWIDTH = (MAX-MIN)/NHIST;

  switch (OPT) {
    case 1:{
        for (int n = 0; n < N_SOLVENT_REAL; ++n) {
        if(Vel[n][0] > MIN && Vel[n][0] < MAX) {
        ++Hx[(size_t)((Vel[n][0] - MIN)/INTERVALWIDTH)];
        }
        if(Vel[n][1] > MIN && Vel[n][1] < MAX) {
        ++Hy[(size_t)((Vel[n][1] - MIN)/INTERVALWIDTH)];
        }
        if(Vel[n][2] > MIN && Vel[n][2] < MAX) {
        ++Hz[(size_t)((Vel[n][2] - MIN)/INTERVALWIDTH)];
        }
	}
        for (int n = N_SOLVENT_REAL; n < NTOT; ++n) {
        if(Vel[n][0] > MIN && Vel[n][0] < MAX) {
        ++Hx[(size_t)((Vel[n][0] - MIN)/INTERVALWIDTH)];
        }
        if(Vel[n][1] > MIN && Vel[n][1] < MAX) {
        ++Hy[(size_t)((Vel[n][1] - MIN)/INTERVALWIDTH)];
        }
        if(Vel[n][2] > MIN && Vel[n][2] < MAX) {
        ++Hz[(size_t)((Vel[n][2] - MIN)/INTERVALWIDTH)];
        }
	}
    } break;
    case 2:
    {
	FILE *Vel_Distr;
	Vel_Distr = fopen("data/vel_distr.txt", "w");
        //fprintf(Vel_Distr, "\nbin size: %f", INTERVALWIDTH);
        //fprintf(Vel_Distr, "\nv(x,y,z)\tProb(vx)\tProb(vy)\tProb(vz)\n");
        for (int k = 0; k < NHIST; ++k) {
          fprintf(Vel_Distr, "%g %lu %lu %lu \n", (MIN + (k + 0.5) * INTERVALWIDTH ), Hx[k], Hy[k], Hz[k]) ;
          }
        fclose(Vel_Distr);
    } break;
  }

}

///Routine to calculate the Temperature in a cell
void calcTemperature(int OPT) {
  static const int KMAX = CYCLES_MPC / FREQ_SAMP;
  static int       K = 0;
  static double    EkinTot[KMAX] = { 0.0 };
  switch (OPT) {
    case 1 :
      if (K < KMAX) {
        for (int n = 0; n < N_SOLVENT_REAL; ++n)
          EkinTot[K] += Vel[n][0] * Vel[n][0] + Vel[n][1] * Vel[n][1] + Vel[n][2] * Vel[n][2];
        for (int n = N_SOLVENT_REAL; n < NTOT; ++n)	
	  EkinTot[K] += (Vel[n][0] * Vel[n][0] + Vel[n][1] * Vel[n][1] + Vel[n][2] * Vel[n][2]) * MASS_MONO;
        ++K;
      }
      break;
    case 2 :
      {
        FILE *temperature;
	temperature = fopen("data/Temperature.txt", "w");
        //fprintf(T_kin, "T_solvent\n");
        for (int k = 0; k < K; ++k) {
          fprintf(temperature, "%g \n" ,EkinTot[k] / (3 * NTOT - 3));
        }
	fclose(temperature);
      }
      break;
  }
}

///output of z-component velocity:
void Bins(unsigned int t, int OPT) {
if(t >= CYCLES_MPC/2 ){

      static const unsigned int TMAX = CYCLES_MPC / FREQ_SAMP;
      static unsigned int T = 0;
      static double V_cm[TMAX][ NCELLS_BINZ ][3]; ///Ausgabe CM-velocities in Bins()
      static double CM[TMAX][ NCELLS_BINZ ][3]; ///Ausgabe CM-position in Bins()
      static unsigned int Ncellbin[TMAX][ NCELLS_BINZ ]; ///Ausgabe particles per cell in Bins()
      static double Temperature[TMAX][NCELLS_BINZ]; ///Ausgabe temperature in cell in Bins()
      static double E_k[TMAX][NCELLS_BINZ]; ///Ausgabe energy in cell in Bins()
      static int Massbin[TMAX][NCELLS_BINZ];
 switch (OPT) {
  case 1:
  if (T < TMAX) {
    ///(2b) Alle Teilchen durchgehen:
    for (size_t n = 0; n < N_SOLVENT_REAL; ++n) {
    ///z-Positionen auf den nächsten ganzen Wert abrunden:
	int idx = floor( FACTOR_BINS * Pos[n][2] );;
        Ncellbin[T][idx]++;
	Massbin[T][idx]++;
	///Summiert die Geschwindigkeiten der Teilchen in einer Box auf:
       	for(size_t d = 0; d < 3; ++d){
            	V_cm[T] [idx][d] += Vel[n][d];
            	CM[T][idx][d] += Pos[n][d];
		}
		E_k[T][idx] += Vel[n][0]*Vel[n][0] +
                    Vel[n][1]*Vel[n][1] + Vel[n][2]*Vel[n][2];
    }
    ///(2c) Alle Monomer-Teilchen durchgehen:
    for (size_t n = N_SOLVENT_REAL; n < NTOT; ++n) {
    ///z-Positionen auf den nächsten ganzen Wert abrunden:
	int idx = floor( FACTOR_BINS * Pos[n][2] );;
        Ncellbin[T][idx]++;
	Massbin[T][idx] += MASS_MONO;
	///Summiert die Geschwindigkeiten der Teilchen in einer Box auf:
       	for(size_t d = 0; d < 3; ++d){
            	V_cm[T] [idx][d] += Vel[n][d] * MASS_MONO;
            	CM[T][idx][d] += Pos[n][d] * MASS_MONO;
		}
		E_k[T][idx] += (Vel[n][0]*Vel[n][0] +
                    Vel[n][1]*Vel[n][1] + Vel[n][2]*Vel[n][2]) * MASS_MONO;
    }

    for(size_t idx = 0; idx < NCELLS_BINZ; ++idx ){
      if(Ncellbin[T][idx] != 0){
      V_cm[T][idx][0] /= Massbin[T][idx];
      V_cm[T][idx][1] /= Massbin[T][idx];
      V_cm[T][idx][2] /= Massbin[T][idx];
      CM[T][idx][0] /= Massbin[T][idx];
      CM[T][idx][1] /= Massbin[T][idx];
      CM[T][idx][2] /= Massbin[T][idx];
      double Vcm2 = 0.0;
      Vcm2 = V_cm[T][idx][0]*V_cm[T][idx][0] +
                    V_cm[T][idx][1]*V_cm[T][idx][1] +
                    V_cm[T][idx][2]*V_cm[T][idx][2];

      double rel_Ekin = 0.0;
      rel_Ekin = E_k[T][idx] - Vcm2 * Massbin[T][idx];
      Temperature[T][idx] = rel_Ekin/(3 * Ncellbin[T][idx] - 3);
      }
    }
  ++T;
  } break;
  case 2:
  { FILE *velocityy;
    FILE *velocityx;
    FILE *velocityz;
    FILE *posz;
    FILE *posy;
    FILE *posx;
    FILE *Nparticles;
    FILE *Temp;
    velocityy = fopen("data/velocityy.txt","w"); ///Ausgabe von V_cm[NCELLS_BINZ] in der Routine Bins()
    velocityx = fopen("data/velocityx.txt","w"); ///Ausgabe von V_cm[NCELLS_BINZ] in der Routine Bins()
    velocityz = fopen("data/velocityz.txt","w"); ///Ausgabe von V_cm[NCELLS_BINZ] in der Routine Bins()
    posz = fopen("data/posz.txt","w"); ///Ausgabe von CM[NCELLS_BINZ] in der Routine Bins()
    posy = fopen("data/posy.txt","w"); ///Ausgabe von CM[NCELLS_BINZ] in der Routine Bins()
    posx = fopen("data/posx.txt","w"); ///Ausgabe von CM[NCELLS_BINZ] in der Routine Bins()
    Nparticles = fopen("data/Nparticles.txt","w"); ///Ausgabe von Ncellbin[NCELLS_BINZ] in der Routine Bins()
    Temp = fopen("data/Temp.txt","w"); ///Ausgabe von Temperature[NCELLS_BINZ] in der Routine Bins()
   for(size_t k = 0; k < T; ++k) {
   for(size_t idx = 0; idx < NCELLS_BINZ; ++idx ){
    fprintf(velocityy, "%f ", V_cm[k][idx][1]);
    fprintf(velocityx, "%f ", V_cm[k][idx][0]);
    fprintf(velocityz, "%f ", V_cm[k][idx][2]);
    fprintf(posz, "%f ", CM[k][idx][2]);
    fprintf(posy, "%f ", CM[k][idx][1]);
    fprintf(posx, "%f ", CM[k][idx][0]);
    fprintf(Nparticles, " %d ", Ncellbin[k][idx]);
    fprintf(Temp, "%f ", Temperature[k][idx]);

    }
    fprintf(velocityy, "\n");
    fprintf(velocityz, "\n");
    fprintf(velocityx, "\n");
    fprintf(posz, "\n");
    fprintf(posy, "\n");
    fprintf(posx, "\n");
    fprintf(Nparticles, "\n");
    fprintf(Temp, "\n");
    }
  fclose(velocityy);
  fclose(velocityx);
  fclose(velocityz);
  fclose(posz);
  fclose(posx);
  fclose(posy);
  fclose(Nparticles);
  fclose(Temp);
  } break;
 }
}

}

///Berechnung der gesamten kin. Energie vor dem Stoss:
void calcEkBEFORE() {

    double Ekin[2] = {0.0, 0.0};

    for(size_t n = 0; n < N_SOLVENT_REAL; ++n){
    Ekin[0] += Vel[n][0]*Vel[n][0] + Vel[n][1]*Vel[n][1] + Vel[n][2]*Vel[n][2];}
    for(size_t n = N_SOLVENT_REAL; n < NTOT; ++n){
    Ekin[1] += (Vel[n][0]*Vel[n][0] + Vel[n][1]*Vel[n][1] + Vel[n][2]*Vel[n][2]) * MASS_MONO;}
    fprintf(Energy," %f ", Ekin[0] + Ekin[1]); ///AUsgabe der kin. Energie
}

///Berechnung der gesamten kin. Energie nach dem Stoss:
void calcEkAFTER() {

    double Ekin[2] = {0.0, 0.0};

    for(size_t n = 0; n < N_SOLVENT_REAL; ++n){
    Ekin[0] += Vel[n][0]*Vel[n][0] + Vel[n][1]*Vel[n][1] + Vel[n][2]*Vel[n][2];}
    for(size_t n = N_SOLVENT_REAL; n < NTOT; ++n){
    Ekin[1] += (Vel[n][0]*Vel[n][0] + Vel[n][1]*Vel[n][1] + Vel[n][2]*Vel[n][2]) * MASS_MONO;}
    fprintf(Energy," %f \n", Ekin[0] + Ekin[1]); ///AUsgabe der kin. Energie
    fprintf(Energy,"\n");
}

void calcTotEnergy(int OPT)   {
  static const int KMAX = CYCLES_MPC / FREQ_SAMP;
  static int       K = 0;
  static double    PotEnergy_BOND_OUTtot[KMAX] = { 0.0 };
  static double    PotEnergy_LJ_OUTtot[KMAX] = { 0.0 };
  static double    KinEnergy_MDtot[KMAX] = { 0.0 };
  static double    KinEnergy_MPCtot[KMAX] = { 0.0 };

  switch (OPT) {
    case 1:
      if (K < KMAX) {
        for(size_t n = 0; n < N_SOLVENT_REAL; ++n) {
          KinEnergy_MPCtot[K] += 0.5 * (Vel[n][0] * Vel[n][0] +
		  Vel[n][1] * Vel[n][1] +
		  Vel[n][2] * Vel[n][2]);
        }
        for(size_t n = N_SOLVENT_REAL; n < NTOT; ++n) {
          KinEnergy_MDtot[K] += FAC_KIN_E* (Vel[n][0] * Vel[n][0] +
		  Vel[n][1] * Vel[n][1] +
		  Vel[n][2] * Vel[n][2]);
        }
        PotEnergy_BOND_OUTtot[K] = PotEnergy_BOND;
	PotEnergy_LJ_OUTtot[K] = PotEnergy_LJ;
        ++K;
      }
    break;
    case 2:
      {
      FILE *EnergyTot;
        EnergyTot = fopen("data/EnergyTot.txt", "w");
        fprintf(EnergyTot, "total energy over time\n");
	fprintf(EnergyTot, "kin. energy solvent,   kin. energy solute,   bounding Pot solute,   LJ Pot solute,   tot energy\n");
        for (int k = 0; k < K; ++k) {
         fprintf(EnergyTot, "%g %g %g %g %g\n", KinEnergy_MPCtot[k], KinEnergy_MDtot[k], PotEnergy_BOND_OUTtot[k], 
		 PotEnergy_LJ_OUTtot[k], KinEnergy_MPCtot[k] + KinEnergy_MDtot[k] + PotEnergy_BOND_OUTtot[k] + PotEnergy_LJ_OUTtot[k]);
         }
        fclose(EnergyTot);
      } break;
  }

}

///Periodic boundary conditions:
void PeriodicBoundaryCond(double *bouPos, int *i) {

    if(bouPos[0] >= BOXLX ){
	bouPos[0] -= BOXLX;
	++i[0];
    }
    else if(bouPos[0] < 0.0) {
	bouPos[0] += BOXLX;
    	--i[0];
    }

    if(bouPos[1] >= BOXLY ){
	bouPos[1] -= BOXLY;
    	++i[1];
    }
    else if(bouPos[1] < 0.0) {
	bouPos[1] += BOXLY;
        --i[1];
    }

    if(bouPos[2] >= BOXLZ  ) {
	bouPos[2] -= BOXLZ;
        ++i[2];
    }
    else if(bouPos[2] < 0.0) {
	bouPos[2] += BOXLZ;
        --i[2];
    }
}

///Anfangsbedingungen 
void Initial_MPC() {

double v_cm[3] = {0.0}; ///Schwerpunktsgeschwindigkeit
double initEkin = 0.0;
///Anfangsbedingungen

    ///Ort real:
    for(size_t n = 0; n < N_SOLVENT_REAL; ++n){///n gibt an welches Teilchen
         ///Ort:
         Pos[n][0] = genrand_real1() * BOXLX;
         Pos[n][1] = genrand_real1() * BOXLY;
         Pos[n][2] = genrand_real1() * BOXLZ;

         ///Geschwindigkeit:
         Vel[n][0] = ran_gauss_ziggurat ();
         Vel[n][1] = ran_gauss_ziggurat ();
         Vel[n][2] = ran_gauss_ziggurat ();
	 ///Um den Gesamtimpuls des Systems auf Null zu setzen:
        ///(1) berechnen der Schwerpunktsgeschwindigkeit
        v_cm[0] += Vel[n][0];
        v_cm[1] += Vel[n][1];
        v_cm[2] += Vel[n][2];
    }

    v_cm[0] /= N_SOLVENT_REAL;//(N_SOLVENT_REAL + N_MONO_TOT * MASS_MONO);
    v_cm[1] /= N_SOLVENT_REAL;//(N_SOLVENT_REAL + N_MONO_TOT * MASS_MONO);
    v_cm[2] /= N_SOLVENT_REAL;//(N_SOLVENT_REAL + N_MONO_TOT * MASS_MONO);

    ///(2) Abziehen der Schwerpunktsgeschwindigkeit von den gegebenen Teilchengeschwindigkeiten:
    for(size_t n= 0; n < N_SOLVENT_REAL; ++n){
    Vel[n][0] -= v_cm[0];
    Vel[n][1] -= v_cm[1];
    Vel[n][2] -= v_cm[2];
    initEkin += Vel[n][0] * Vel[n][0] + Vel[n][1] * Vel[n][1] + Vel[n][2] * Vel[n][2];
    }///nun sind die Teilchen auch zero-drifting,

    double rescFAC = sqrt((3 * N_SOLVENT_REAL - 3 ) * k_BT / initEkin);
    for (int n = 0; n < N_SOLVENT_REAL; ++n) {
      Vel[n][0] *= rescFAC;
      Vel[n][1] *= rescFAC;
      Vel[n][2] *= rescFAC;
    }

}

///Streaming-step:
void Streaming() {

   for (int n = 0; n < N_SOLVENT_REAL; ++n){

    Pos[n][0] += DELTA_T_MPC * Vel[n][0];
    Pos[n][1] += DELTA_T_MPC * Vel[n][1];
    Pos[n][2] += DELTA_T_MPC * Vel[n][2];

    ///Randbedingungen anwenden:
	PeriodicBoundaryCond(Pos[n], N_PBC[n]);
	
   }
   //VelVerlet(); 
}

void calcLinMom_MD(int OPT)  {
  static const int KMAX = CYCLES_MPC / FREQ_SAMP;
  static int       K = 0;
  static double    LinMom[KMAX][3] = { 0.0 };

  switch (OPT) {
    case 1:
      if (K < KMAX) {
        for(size_t n = 0; n < N_SOLVENT_REAL; ++n) {
        LinMom[K][0] += Vel[n][0];
        LinMom[K][1] += Vel[n][1];
        LinMom[K][2] += Vel[n][2];
        }
        for(size_t n = N_SOLVENT_REAL; n < NTOT; ++n) {
        LinMom[K][0] += MASS_MONO * Vel[n][0];
        LinMom[K][1] += MASS_MONO * Vel[n][1];
        LinMom[K][2] += MASS_MONO * Vel[n][2];
        }
      ++K;
      }
    break;
    case 2:
      {
      FILE *LinMom_MDFile;
        LinMom_MDFile = fopen("data/LinMom_MD.txt", "w");
        fprintf(LinMom_MDFile, "total linear momentum over time");
        for (int k = 0; k < K; ++k) {
         fprintf(LinMom_MDFile, "\n %g %g %g", LinMom[k][0], LinMom[k][1], LinMom[k][2]);
         }
        fclose(LinMom_MDFile);
      } break;
  }

}

///Collision step:
void Collision() {

///Random shift (nach jedem Zeitschritt neu):

        Orig[0] = genrand_real1()-RANDOM_SHIFT;///random shift um -a/2...CELL_SIZE a/2
        Orig[1] = genrand_real1()-RANDOM_SHIFT;
        Orig[2] = genrand_real1()-RANDOM_SHIFT;

///Teilchen nach Zellen sortieren:

        ///(1) Nach jedem Zeitschritt die arrays NperCELL, Vcm, Ek, Pcm, I, deltaL, Lbefore, Lafter
	///wieder leeren, sonst akkumulieren sich die Werte:
        for(size_t idx=0; idx < NCELLS; ++idx){

        NperCELL[idx]=0;

        Vcm[idx][0]=0.0;
        Vcm[idx][1]=0.0;
        Vcm[idx][2]=0.0;
	Mass[idx] = 0;
        Ek[idx]=0.0;

	Pbefore[idx][0] = 0.0;
	Pbefore[idx][1] = 0.0;
	Pbefore[idx][2] = 0.0;

	Pafter[idx][0] = 0.0;
	Pafter[idx][1] = 0.0;
	Pafter[idx][2] = 0.0;
	}

    ///(2a) Alle Solvent-Teilchen durchgehen:
    for (size_t n = 0; n < N_SOLVENT_REAL; ++n) {

    	///Positionen auf den nächsten ganzen Wert abrunden:
        int rund[3];
        rund[0] = floor(Pos[n][0]-Orig[0]);///Box wird auch um den random shift verschoben
        rund[1] = floor(Pos[n][1]-Orig[1]);
        rund[2] = floor(Pos[n][2]-Orig[2]);

        ///"Randbedingungen" für die Zellenindizes und die Hilfspositionen:
        if (rund[0] < 0)
        {rund[0] += NCELLSX;
        }
        else if (rund[0] >= NCELLSX)
        {rund[0] -= NCELLSX;
        }
        if (rund[1] < 0)
        {rund[1] += NCELLSY;
        }
        else if (rund[1] >= NCELLSY)
        {rund[1] -= NCELLSY;
        }
        if (rund[2] < 0)
        {rund[2] += NCELLSZ;
        }
        else if (rund[2] >= NCELLSZ)
        {rund[2] -= NCELLSZ;
        }
	///Zellenindex bestimmen:
        int idx = rund[0] + (rund[1] + rund[2] * NCELLSY) * NCELLSX;

        Cell[n] = idx; ///Vektor, der die Indizes der jeweiligen Teilchen enthält
	Mass[idx]++;
        NperCELL[idx]++; ///Anzahl der Teilchen pro Zelle hochzaehlen

	///Summiert die Geschwindigkeiten der Teilchen in den einzelnen Zellen auf:
        Vcm[idx][0] += Vel[n][0];
	Vcm[idx][1] += Vel[n][1];
	Vcm[idx][2] += Vel[n][2];
	///Summiert die kin Energie der Teilchen in den einzelnen Zellen auf:
        Ek[idx] += Vel[n][0]*Vel[n][0] +
            Vel[n][1]*Vel[n][1] + Vel[n][2]*Vel[n][2];
    }
    ///(2b) Alle Monomer-Teilchen durchgehen:
    for (size_t n = N_SOLVENT_REAL; n < NTOT; ++n) {

      ///Positionen auf den nächsten ganzen Wert abrunden:
        int rund[3];
        rund[0] = int(Pos[n][0]-Orig[0]);///Box wird auch um den random shift verschoben
        rund[1] = int(Pos[n][1]-Orig[1]);
        rund[2] = int(Pos[n][2]-Orig[2]);

        ///"Randbedingungen" für die Zellenindizes und die Hilfspositionen:
        if (rund[0] < 0)
        {rund[0] += NCELLSX;
        }
        else if (rund[0] >= NCELLSX)
        {rund[0] -= NCELLSX;
        }
        if (rund[1] < 0)
        {rund[1] += NCELLSY;
        }
        else if (rund[1] >= NCELLSY)
        {rund[1] -= NCELLSY;
        }
        if (rund[2] < 0)
        {rund[2] += NCELLSZ;
        }
        else if (rund[2] >= NCELLSZ)
        {rund[2] -= NCELLSZ;
        }
	///Zellenindex bestimmen:
        int idx = rund[0] + (rund[1] + rund[2] * NCELLSY) * NCELLSX;

        Cell[n] = idx; ///Vektor, der die Indizes der jeweiligen Teilchen enthält
	Mass[idx] += MASS_MONO;
        NperCELL[idx]++; ///Anzahl der Teilchen pro Zelle hochzaehlen

	///Summiert die Geschwindigkeiten der Teilchen in den einzelnen Zellen auf:
        Vcm[idx][0] += Vel[n][0] * MASS_MONO;
	Vcm[idx][1] += Vel[n][1] * MASS_MONO;
	Vcm[idx][2] += Vel[n][2] * MASS_MONO;
	///Summiert die kin Energie der Teilchen in den einzelnen Zellen auf:
        Ek[idx] += (Vel[n][0]*Vel[n][0] +
            Vel[n][1]*Vel[n][1] + Vel[n][2]*Vel[n][2]) * MASS_MONO;
    }


        ///(3) Alle Zellen durchgehen:
        for(size_t idx = 0; idx < NCELLS; ++idx){

          if (Mass[idx] > 0) { ///Nur wenn genug Teilchen in einer Box sind,...
            double Vcm2 = 0.0;

            Vcm[idx][0] /= Mass[idx]; ///Berechnung der Schwerpunktsgeschwindigkeit
            Vcm[idx][1] /= Mass[idx]; ///Berechnung der Schwerpunktsgeschwindigkeit
            Vcm[idx][2] /= Mass[idx]; ///Berechnung der Schwerpunktsgeschwindigkeit

	    ///Berechnung der kin Energie der Schwerpunktsbewegung:
            Vcm2 = Vcm[idx][0]*Vcm[idx][0] +
            	        Vcm[idx][1]*Vcm[idx][1] +
                        Vcm[idx][2]*Vcm[idx][2];

	    ///Berechnung der relativen kin Energie:
            double rel_Ek = Ek[idx] - Vcm2 * Mass[idx];

            if( rel_Ek < 1.0e-8) {
            ///keine Drehung, wenn bloss ein Teilchen in der Zelle ist, oder die kin Energie auch der des Schwerpunkts entspricht:
            	fac[idx] = 1.0;
     	   	D_rot[idx][0][0] = 1.0;
            	D_rot[idx][0][1] = 0.0;
   	     	D_rot[idx][0][2] = 0.0;
   	     	D_rot[idx][1][0] = 0.0;
   	     	D_rot[idx][1][1] = 1.0;
   	     	D_rot[idx][1][2] = 0.0;
            	D_rot[idx][2][1] = 0.0;
       		D_rot[idx][2][2] = 1.0;
		}
	    else {
	    ///Berechnung der rel kin Energie aus der Gammaverteilung:
	    double ran_rel_Ek = rgamma( NperCELL[idx] );
	    ///Berechnung des Faktors fuer den canonical thermostat:
	    if(Thermostat == 1) {
	    fac[idx] = sqrt((ran_rel_Ek + ran_rel_Ek)/rel_Ek);
	    }
	    else if (Thermostat == 0) {
	    fac[idx] = 1.0;
	    }
            phi=2.0*M_PI*genrand_real1(); ///Zufallszahlen für phi
            theta=2.0*genrand_real1()-1.0; ///Zufallszahlen für theta

            R_x = sqrt(1.0-theta*theta)*cos(phi); ///Für Matrix D_rot
            R_y = sqrt(1.0-theta*theta)*sin(phi); ///Für Matrix D_rot
            R_z =theta; ///Für Matrix D_rot

            ///Elemente der Matrix D_rot:
            D_rot[idx][0][0] = R_x*R_x+(1.0-R_x*R_x)*c ;
            D_rot[idx][0][1] = R_x*R_y*(1.0-c) - R_z*s;
            D_rot[idx][0][2] = R_x*R_z*(1.0-c) + R_y*s;
            D_rot[idx][1][0] = R_x*R_y*(1.0-c) + R_z*s;
            D_rot[idx][1][1] = R_y*R_y+(1.0-R_y*R_y)*c ;
            D_rot[idx][1][2] = R_y*R_z*(1.0-c) - R_x*s;
            D_rot[idx][2][0] = R_x*R_z*(1.0-c) - R_y*s;
            D_rot[idx][2][1] = R_y*R_z*(1.0-c) + R_x*s;
            D_rot[idx][2][2] = R_z*R_z+(1.0-R_z*R_z)*c ;
        }
	}
	}
	///Berechnung der Impulses in den Zellen:
	calcLinMomBefore();

        ///der eigentliche Collision step:
        for (size_t n = 0; n < NTOT; ++n) {
        int idx = Cell[n]; ///Zelle des Teilchens n wird aus dem Vektor Cell[NCELLS] entnommen
        double vrel[3];
                for (int d = 0; d < 3; ++d){
                    vrel[d] = (Vel[n][d] - Vcm[idx][d]) * fac[idx];
                    }
            Vel[n][0] = D_rot[idx][0][0] * vrel[0] +
			D_rot[idx][0][1] * vrel[1] +
			D_rot[idx][0][2] * vrel[2] + Vcm[idx][0];
            Vel[n][1] = D_rot[idx][1][0] * vrel[0] +
			D_rot[idx][1][1] * vrel[1] +
			D_rot[idx][1][2] * vrel[2] + Vcm[idx][1]; 
            Vel[n][2] = D_rot[idx][2][0] * vrel[0] +
			D_rot[idx][2][1] * vrel[1] +
			D_rot[idx][2][2] * vrel[2] + Vcm[idx][2];
	}
	calcLinMomAfter();
}
//////////////////////////////////////////////

void mpc_get_solute_buffer() {
 for(size_t n = N_SOLVENT_REAL; n < NTOT; ++n) {
  int o = n - N_SOLVENT_REAL;
  Pos[n][0] = Pos_MD[o][0]; 
  Pos[n][1] = Pos_MD[o][1];  
  Pos[n][2] = Pos_MD[o][2]; 
  Vel[n][0] = Vel_MD[o][0]; 
  Vel[n][1] = Vel_MD[o][1];  
  Vel[n][2] = Vel_MD[o][2]; 
 }
}

void md_get_solute_buffer()  {
  for(size_t n = 0; n < N_MONO_TOT; ++n) {
    int o = n + N_SOLVENT_REAL;
    Pos_MD[n][0] = Pos[o][0]; 
    Pos_MD[n][1] = Pos[o][1];  
    Pos_MD[n][2] = Pos[o][2]; 
    Vel_MD[n][0] = Vel[o][0]; 
    Vel_MD[n][1] = Vel[o][1];  
    Vel_MD[n][2] = Vel[o][2]; 
  }
}

void runMPC() {
    Streaming();

      calcEkBEFORE();///gibt die Gesamt-kin. Energie des Systems aus
    Collision();
      calcEkAFTER();///gibt die Gesamt-kin. Energie des Systems aus
}

#endif /* MPC_H */
